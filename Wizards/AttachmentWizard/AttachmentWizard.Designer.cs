﻿namespace AttachmentWizard
{
    partial class AttachmentWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AttachmentWizard));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ux_buttonAddLink = new System.Windows.Forms.Button();
            this.ux_progressbarLoading = new System.Windows.Forms.ProgressBar();
            this.uz_buttonCancel2 = new System.Windows.Forms.Button();
            this.ux_buttonAttachAll = new System.Windows.Forms.Button();
            this.ux_buttonSelectFromRepository = new System.Windows.Forms.Button();
            this.ux_buttonSelectFromPC = new System.Windows.Forms.Button();
            this.ux_datagridviewNewAttachment = new System.Windows.Forms.DataGridView();
            this.file_source = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.category_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.file_physical_path = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.virtual_path = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hash = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.is_visible = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ux_progressbarAttachmentsByInventory = new System.Windows.Forms.ProgressBar();
            this.ux_buttonDelete = new System.Windows.Forms.Button();
            this.ux_buttonDownload = new System.Windows.Forms.Button();
            this.ux_buttonCancel1 = new System.Windows.Forms.Button();
            this.ux_buttonOpen = new System.Windows.Forms.Button();
            this.ux_datagridviewAttachmentsByInventory = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewNewAttachment)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewAttachmentsByInventory)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ux_buttonAddLink);
            this.tabPage2.Controls.Add(this.ux_progressbarLoading);
            this.tabPage2.Controls.Add(this.uz_buttonCancel2);
            this.tabPage2.Controls.Add(this.ux_buttonAttachAll);
            this.tabPage2.Controls.Add(this.ux_buttonSelectFromRepository);
            this.tabPage2.Controls.Add(this.ux_buttonSelectFromPC);
            this.tabPage2.Controls.Add(this.ux_datagridviewNewAttachment);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(928, 448);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "New Attachment";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ux_buttonAddLink
            // 
            this.ux_buttonAddLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonAddLink.Location = new System.Drawing.Point(457, 18);
            this.ux_buttonAddLink.Name = "ux_buttonAddLink";
            this.ux_buttonAddLink.Size = new System.Drawing.Size(147, 23);
            this.ux_buttonAddLink.TabIndex = 8;
            this.ux_buttonAddLink.Text = "Add Link";
            this.ux_buttonAddLink.UseVisualStyleBackColor = true;
            this.ux_buttonAddLink.Click += new System.EventHandler(this.ux_buttonAddLink_Click);
            // 
            // ux_progressbarLoading
            // 
            this.ux_progressbarLoading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_progressbarLoading.Location = new System.Drawing.Point(18, 400);
            this.ux_progressbarLoading.Name = "ux_progressbarLoading";
            this.ux_progressbarLoading.Size = new System.Drawing.Size(586, 23);
            this.ux_progressbarLoading.TabIndex = 7;
            this.ux_progressbarLoading.Visible = false;
            // 
            // uz_buttonCancel2
            // 
            this.uz_buttonCancel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uz_buttonCancel2.Location = new System.Drawing.Point(763, 400);
            this.uz_buttonCancel2.Name = "uz_buttonCancel2";
            this.uz_buttonCancel2.Size = new System.Drawing.Size(147, 23);
            this.uz_buttonCancel2.TabIndex = 6;
            this.uz_buttonCancel2.Text = "Close";
            this.uz_buttonCancel2.UseVisualStyleBackColor = true;
            this.uz_buttonCancel2.Click += new System.EventHandler(this.uz_buttonCancel_Click);
            // 
            // ux_buttonAttachAll
            // 
            this.ux_buttonAttachAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonAttachAll.Location = new System.Drawing.Point(610, 400);
            this.ux_buttonAttachAll.Name = "ux_buttonAttachAll";
            this.ux_buttonAttachAll.Size = new System.Drawing.Size(147, 23);
            this.ux_buttonAttachAll.TabIndex = 5;
            this.ux_buttonAttachAll.Text = "Attach or link files";
            this.ux_buttonAttachAll.UseVisualStyleBackColor = true;
            this.ux_buttonAttachAll.Click += new System.EventHandler(this.ux_buttonAttachAll_Click);
            // 
            // ux_buttonSelectFromRepository
            // 
            this.ux_buttonSelectFromRepository.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSelectFromRepository.Location = new System.Drawing.Point(763, 18);
            this.ux_buttonSelectFromRepository.Name = "ux_buttonSelectFromRepository";
            this.ux_buttonSelectFromRepository.Size = new System.Drawing.Size(147, 23);
            this.ux_buttonSelectFromRepository.TabIndex = 4;
            this.ux_buttonSelectFromRepository.Text = "Select file from Repository";
            this.ux_buttonSelectFromRepository.UseVisualStyleBackColor = true;
            this.ux_buttonSelectFromRepository.Click += new System.EventHandler(this.ux_buttonSelectFromRepository_Click);
            // 
            // ux_buttonSelectFromPC
            // 
            this.ux_buttonSelectFromPC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSelectFromPC.Location = new System.Drawing.Point(610, 18);
            this.ux_buttonSelectFromPC.Name = "ux_buttonSelectFromPC";
            this.ux_buttonSelectFromPC.Size = new System.Drawing.Size(147, 23);
            this.ux_buttonSelectFromPC.TabIndex = 3;
            this.ux_buttonSelectFromPC.Text = "Select file from PC";
            this.ux_buttonSelectFromPC.UseVisualStyleBackColor = true;
            this.ux_buttonSelectFromPC.Click += new System.EventHandler(this.ux_buttonSelectFromPC_Click);
            // 
            // ux_datagridviewNewAttachment
            // 
            this.ux_datagridviewNewAttachment.AllowUserToAddRows = false;
            this.ux_datagridviewNewAttachment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewNewAttachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewNewAttachment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.file_source,
            this.category_code,
            this.title,
            this.description,
            this.file_physical_path,
            this.virtual_path,
            this.size,
            this.hash,
            this.is_visible,
            this.note});
            this.ux_datagridviewNewAttachment.Location = new System.Drawing.Point(20, 48);
            this.ux_datagridviewNewAttachment.Name = "ux_datagridviewNewAttachment";
            this.ux_datagridviewNewAttachment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ux_datagridviewNewAttachment.Size = new System.Drawing.Size(890, 345);
            this.ux_datagridviewNewAttachment.TabIndex = 2;
            // 
            // file_source
            // 
            this.file_source.HeaderText = "Source";
            this.file_source.Name = "file_source";
            this.file_source.ReadOnly = true;
            // 
            // category_code
            // 
            this.category_code.HeaderText = "Type";
            this.category_code.Name = "category_code";
            // 
            // title
            // 
            this.title.HeaderText = "Title";
            this.title.Name = "title";
            // 
            // description
            // 
            this.description.HeaderText = "Description";
            this.description.Name = "description";
            // 
            // file_physical_path
            // 
            this.file_physical_path.HeaderText = "Physical path";
            this.file_physical_path.Name = "file_physical_path";
            this.file_physical_path.ReadOnly = true;
            // 
            // virtual_path
            // 
            this.virtual_path.HeaderText = "Virtual path";
            this.virtual_path.Name = "virtual_path";
            this.virtual_path.ReadOnly = true;
            // 
            // size
            // 
            this.size.HeaderText = "Size";
            this.size.Name = "size";
            this.size.ReadOnly = true;
            // 
            // hash
            // 
            this.hash.HeaderText = "Hash";
            this.hash.Name = "hash";
            this.hash.ReadOnly = true;
            this.hash.Visible = false;
            // 
            // is_visible
            // 
            this.is_visible.HeaderText = "Is web visible";
            this.is_visible.Name = "is_visible";
            // 
            // note
            // 
            this.note.HeaderText = "note";
            this.note.Name = "note";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ux_progressbarAttachmentsByInventory);
            this.tabPage1.Controls.Add(this.ux_buttonDelete);
            this.tabPage1.Controls.Add(this.ux_buttonDownload);
            this.tabPage1.Controls.Add(this.ux_buttonCancel1);
            this.tabPage1.Controls.Add(this.ux_buttonOpen);
            this.tabPage1.Controls.Add(this.ux_datagridviewAttachmentsByInventory);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(928, 448);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Attachments by inventory";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ux_progressbarAttachmentsByInventory
            // 
            this.ux_progressbarAttachmentsByInventory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_progressbarAttachmentsByInventory.Location = new System.Drawing.Point(321, 410);
            this.ux_progressbarAttachmentsByInventory.Name = "ux_progressbarAttachmentsByInventory";
            this.ux_progressbarAttachmentsByInventory.Size = new System.Drawing.Size(280, 23);
            this.ux_progressbarAttachmentsByInventory.TabIndex = 5;
            // 
            // ux_buttonDelete
            // 
            this.ux_buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonDelete.Location = new System.Drawing.Point(173, 410);
            this.ux_buttonDelete.Name = "ux_buttonDelete";
            this.ux_buttonDelete.Size = new System.Drawing.Size(142, 23);
            this.ux_buttonDelete.TabIndex = 4;
            this.ux_buttonDelete.Text = "Delete";
            this.ux_buttonDelete.UseVisualStyleBackColor = true;
            this.ux_buttonDelete.Click += new System.EventHandler(this.ux_buttonDelete_Click);
            // 
            // ux_buttonDownload
            // 
            this.ux_buttonDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonDownload.Location = new System.Drawing.Point(19, 410);
            this.ux_buttonDownload.Name = "ux_buttonDownload";
            this.ux_buttonDownload.Size = new System.Drawing.Size(142, 23);
            this.ux_buttonDownload.TabIndex = 3;
            this.ux_buttonDownload.Text = "Download";
            this.ux_buttonDownload.UseVisualStyleBackColor = true;
            this.ux_buttonDownload.Click += new System.EventHandler(this.ux_buttonDownloadAll_Click);
            // 
            // ux_buttonCancel1
            // 
            this.ux_buttonCancel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancel1.Location = new System.Drawing.Point(761, 410);
            this.ux_buttonCancel1.Name = "ux_buttonCancel1";
            this.ux_buttonCancel1.Size = new System.Drawing.Size(142, 23);
            this.ux_buttonCancel1.TabIndex = 2;
            this.ux_buttonCancel1.Text = "Close";
            this.ux_buttonCancel1.UseVisualStyleBackColor = true;
            this.ux_buttonCancel1.Click += new System.EventHandler(this.uz_buttonCancel_Click);
            // 
            // ux_buttonOpen
            // 
            this.ux_buttonOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonOpen.Location = new System.Drawing.Point(607, 410);
            this.ux_buttonOpen.Name = "ux_buttonOpen";
            this.ux_buttonOpen.Size = new System.Drawing.Size(142, 23);
            this.ux_buttonOpen.TabIndex = 1;
            this.ux_buttonOpen.Text = "Open File";
            this.ux_buttonOpen.UseVisualStyleBackColor = true;
            this.ux_buttonOpen.Click += new System.EventHandler(this.ux_buttonOpen_Click);
            // 
            // ux_datagridviewAttachmentsByInventory
            // 
            this.ux_datagridviewAttachmentsByInventory.AllowUserToAddRows = false;
            this.ux_datagridviewAttachmentsByInventory.AllowUserToDeleteRows = false;
            this.ux_datagridviewAttachmentsByInventory.AllowUserToResizeRows = false;
            this.ux_datagridviewAttachmentsByInventory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewAttachmentsByInventory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewAttachmentsByInventory.Location = new System.Drawing.Point(16, 19);
            this.ux_datagridviewAttachmentsByInventory.Name = "ux_datagridviewAttachmentsByInventory";
            this.ux_datagridviewAttachmentsByInventory.ReadOnly = true;
            this.ux_datagridviewAttachmentsByInventory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ux_datagridviewAttachmentsByInventory.Size = new System.Drawing.Size(884, 382);
            this.ux_datagridviewAttachmentsByInventory.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(936, 474);
            this.tabControl1.TabIndex = 0;
            // 
            // AttachmentWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 474);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AttachmentWizard";
            this.Text = "Attachment Wizard";
            this.Load += new System.EventHandler(this.AttachmentWizard_Load);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewNewAttachment)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewAttachmentsByInventory)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ProgressBar ux_progressbarLoading;
        private System.Windows.Forms.Button uz_buttonCancel2;
        private System.Windows.Forms.Button ux_buttonAttachAll;
        private System.Windows.Forms.Button ux_buttonSelectFromRepository;
        private System.Windows.Forms.Button ux_buttonSelectFromPC;
        private System.Windows.Forms.DataGridView ux_datagridviewNewAttachment;
        private System.Windows.Forms.DataGridViewTextBoxColumn file_source;
        private System.Windows.Forms.DataGridViewTextBoxColumn category_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn title;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewTextBoxColumn file_physical_path;
        private System.Windows.Forms.DataGridViewTextBoxColumn virtual_path;
        private System.Windows.Forms.DataGridViewTextBoxColumn size;
        private System.Windows.Forms.DataGridViewTextBoxColumn hash;
        private System.Windows.Forms.DataGridViewTextBoxColumn is_visible;
        private System.Windows.Forms.DataGridViewTextBoxColumn note;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ProgressBar ux_progressbarAttachmentsByInventory;
        private System.Windows.Forms.Button ux_buttonDelete;
        private System.Windows.Forms.Button ux_buttonDownload;
        private System.Windows.Forms.Button ux_buttonCancel1;
        private System.Windows.Forms.Button ux_buttonOpen;
        private System.Windows.Forms.DataGridView ux_datagridviewAttachmentsByInventory;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button ux_buttonAddLink;
    }
}