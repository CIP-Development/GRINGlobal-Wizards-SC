﻿namespace AccessionBatchWizard
{
    partial class BatchWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BatchWizard));
            this.ux_Button_Close = new System.Windows.Forms.Button();
            this.ux_Button_Add_Descriptor = new System.Windows.Forms.Button();
            this.ux_listBoxCategory = new System.Windows.Forms.ListBox();
            this.ux_listBoxTrait = new System.Windows.Forms.ListBox();
            this.ux_groupBox_Descriptors = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ux_listBoxCrop = new System.Windows.Forms.ListBox();
            this.ux_buttonRemoveDescriptor = new System.Windows.Forms.Button();
            this.ux_listBoxDescriptors = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ux_progressBarWorking = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ux_buttonClear = new System.Windows.Forms.Button();
            this.ux_buttonTemplate = new System.Windows.Forms.Button();
            this.ux_labelMessage = new System.Windows.Forms.Label();
            this.ux_buttonSaveData = new System.Windows.Forms.Button();
            this.ux_datagridviewSheet = new System.Windows.Forms.DataGridView();
            this.ux_groupboxSheet = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ux_comboboxMethod = new System.Windows.Forms.ComboBox();
            this.ux_checkBoxShowOnlyRowsWithError = new System.Windows.Forms.CheckBox();
            this.ux_checkboxIsArchived = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.contextMenuStripMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyHeadersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateLookupTablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_textBoxSep1 = new System.Windows.Forms.TextBox();
            this.ux_textBoxSep2 = new System.Windows.Forms.TextBox();
            this.ux_groupBox_Descriptors.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewSheet)).BeginInit();
            this.ux_groupboxSheet.SuspendLayout();
            this.contextMenuStripMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_Button_Close
            // 
            this.ux_Button_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Button_Close.Location = new System.Drawing.Point(825, 6);
            this.ux_Button_Close.Name = "ux_Button_Close";
            this.ux_Button_Close.Size = new System.Drawing.Size(90, 23);
            this.ux_Button_Close.TabIndex = 0;
            this.ux_Button_Close.Text = "Close";
            this.ux_Button_Close.UseVisualStyleBackColor = true;
            this.ux_Button_Close.Click += new System.EventHandler(this.ux_Button_Close_Click);
            // 
            // ux_Button_Add_Descriptor
            // 
            this.ux_Button_Add_Descriptor.Location = new System.Drawing.Point(510, 166);
            this.ux_Button_Add_Descriptor.Name = "ux_Button_Add_Descriptor";
            this.ux_Button_Add_Descriptor.Size = new System.Drawing.Size(116, 23);
            this.ux_Button_Add_Descriptor.TabIndex = 2;
            this.ux_Button_Add_Descriptor.Text = "Add Descriptor";
            this.ux_Button_Add_Descriptor.UseVisualStyleBackColor = true;
            this.ux_Button_Add_Descriptor.Click += new System.EventHandler(this.ux_Button_Add_Descriptor_Click);
            // 
            // ux_listBoxCategory
            // 
            this.ux_listBoxCategory.FormattingEnabled = true;
            this.ux_listBoxCategory.Location = new System.Drawing.Point(173, 39);
            this.ux_listBoxCategory.Name = "ux_listBoxCategory";
            this.ux_listBoxCategory.Size = new System.Drawing.Size(155, 121);
            this.ux_listBoxCategory.TabIndex = 9;
            this.ux_listBoxCategory.SelectedIndexChanged += new System.EventHandler(this.ux_listBoxCategory_SelectedIndexChanged);
            // 
            // ux_listBoxTrait
            // 
            this.ux_listBoxTrait.FormattingEnabled = true;
            this.ux_listBoxTrait.Location = new System.Drawing.Point(344, 39);
            this.ux_listBoxTrait.Name = "ux_listBoxTrait";
            this.ux_listBoxTrait.Size = new System.Drawing.Size(282, 121);
            this.ux_listBoxTrait.TabIndex = 10;
            this.ux_listBoxTrait.DoubleClick += new System.EventHandler(this.ux_listBoxTrait_DoubleClick);
            // 
            // ux_groupBox_Descriptors
            // 
            this.ux_groupBox_Descriptors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupBox_Descriptors.Controls.Add(this.label6);
            this.ux_groupBox_Descriptors.Controls.Add(this.label5);
            this.ux_groupBox_Descriptors.Controls.Add(this.label4);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_listBoxCrop);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_buttonRemoveDescriptor);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_listBoxDescriptors);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_listBoxTrait);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_Button_Add_Descriptor);
            this.ux_groupBox_Descriptors.Controls.Add(this.ux_listBoxCategory);
            this.ux_groupBox_Descriptors.Location = new System.Drawing.Point(13, 12);
            this.ux_groupBox_Descriptors.Name = "ux_groupBox_Descriptors";
            this.ux_groupBox_Descriptors.Size = new System.Drawing.Size(902, 197);
            this.ux_groupBox_Descriptors.TabIndex = 11;
            this.ux_groupBox_Descriptors.TabStop = false;
            this.ux_groupBox_Descriptors.Text = "Select descriptors";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(341, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Descriptors";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(170, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Group";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Crop";
            // 
            // ux_listBoxCrop
            // 
            this.ux_listBoxCrop.FormattingEnabled = true;
            this.ux_listBoxCrop.Location = new System.Drawing.Point(17, 39);
            this.ux_listBoxCrop.Name = "ux_listBoxCrop";
            this.ux_listBoxCrop.Size = new System.Drawing.Size(143, 121);
            this.ux_listBoxCrop.TabIndex = 13;
            this.ux_listBoxCrop.SelectedIndexChanged += new System.EventHandler(this.ux_listBoxCrop_SelectedIndexChanged);
            // 
            // ux_buttonRemoveDescriptor
            // 
            this.ux_buttonRemoveDescriptor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonRemoveDescriptor.Location = new System.Drawing.Point(761, 166);
            this.ux_buttonRemoveDescriptor.Name = "ux_buttonRemoveDescriptor";
            this.ux_buttonRemoveDescriptor.Size = new System.Drawing.Size(127, 23);
            this.ux_buttonRemoveDescriptor.TabIndex = 12;
            this.ux_buttonRemoveDescriptor.Text = "Remove Descriptor";
            this.ux_buttonRemoveDescriptor.UseVisualStyleBackColor = true;
            this.ux_buttonRemoveDescriptor.Click += new System.EventHandler(this.ux_buttonRemoveDescriptor_Click);
            // 
            // ux_listBoxDescriptors
            // 
            this.ux_listBoxDescriptors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_listBoxDescriptors.FormattingEnabled = true;
            this.ux_listBoxDescriptors.Location = new System.Drawing.Point(648, 39);
            this.ux_listBoxDescriptors.Name = "ux_listBoxDescriptors";
            this.ux_listBoxDescriptors.Size = new System.Drawing.Size(239, 121);
            this.ux_listBoxDescriptors.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(678, 339);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(224, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Red Cell    : Errors were detected while saving";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(469, 339);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "White Cell  : It is not necessary to change";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(244, 339);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Blue Cell : Data has been successfully saved";
            // 
            // ux_progressBarWorking
            // 
            this.ux_progressBarWorking.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_progressBarWorking.Location = new System.Drawing.Point(430, 6);
            this.ux_progressBarWorking.Name = "ux_progressBarWorking";
            this.ux_progressBarWorking.Size = new System.Drawing.Size(264, 23);
            this.ux_progressBarWorking.TabIndex = 15;
            this.ux_progressBarWorking.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ux_buttonClear);
            this.panel1.Controls.Add(this.ux_buttonTemplate);
            this.panel1.Controls.Add(this.ux_labelMessage);
            this.panel1.Controls.Add(this.ux_buttonSaveData);
            this.panel1.Controls.Add(this.ux_Button_Close);
            this.panel1.Controls.Add(this.ux_progressBarWorking);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 578);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(928, 36);
            this.panel1.TabIndex = 17;
            // 
            // ux_buttonClear
            // 
            this.ux_buttonClear.Location = new System.Drawing.Point(323, 6);
            this.ux_buttonClear.Name = "ux_buttonClear";
            this.ux_buttonClear.Size = new System.Drawing.Size(91, 23);
            this.ux_buttonClear.TabIndex = 23;
            this.ux_buttonClear.Text = "Clear";
            this.ux_buttonClear.UseVisualStyleBackColor = true;
            this.ux_buttonClear.Click += new System.EventHandler(this.ux_buttonClear_Click);
            // 
            // ux_buttonTemplate
            // 
            this.ux_buttonTemplate.Location = new System.Drawing.Point(178, 6);
            this.ux_buttonTemplate.Name = "ux_buttonTemplate";
            this.ux_buttonTemplate.Size = new System.Drawing.Size(131, 23);
            this.ux_buttonTemplate.TabIndex = 22;
            this.ux_buttonTemplate.Text = "Manage Templates";
            this.ux_buttonTemplate.UseVisualStyleBackColor = true;
            this.ux_buttonTemplate.Click += new System.EventHandler(this.ux_buttonTemplate_Click);
            // 
            // ux_labelMessage
            // 
            this.ux_labelMessage.AutoSize = true;
            this.ux_labelMessage.Location = new System.Drawing.Point(12, 11);
            this.ux_labelMessage.MinimumSize = new System.Drawing.Size(150, 0);
            this.ux_labelMessage.Name = "ux_labelMessage";
            this.ux_labelMessage.Size = new System.Drawing.Size(150, 13);
            this.ux_labelMessage.TabIndex = 19;
            // 
            // ux_buttonSaveData
            // 
            this.ux_buttonSaveData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSaveData.Location = new System.Drawing.Point(713, 6);
            this.ux_buttonSaveData.Name = "ux_buttonSaveData";
            this.ux_buttonSaveData.Size = new System.Drawing.Size(96, 23);
            this.ux_buttonSaveData.TabIndex = 18;
            this.ux_buttonSaveData.Text = "Save Data";
            this.ux_buttonSaveData.UseVisualStyleBackColor = true;
            this.ux_buttonSaveData.Click += new System.EventHandler(this.ux_buttonSaveData_Click);
            // 
            // ux_datagridviewSheet
            // 
            this.ux_datagridviewSheet.AllowUserToAddRows = false;
            this.ux_datagridviewSheet.AllowUserToOrderColumns = true;
            this.ux_datagridviewSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewSheet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ux_datagridviewSheet.Location = new System.Drawing.Point(6, 43);
            this.ux_datagridviewSheet.Name = "ux_datagridviewSheet";
            this.ux_datagridviewSheet.Size = new System.Drawing.Size(891, 290);
            this.ux_datagridviewSheet.TabIndex = 2;
            this.ux_datagridviewSheet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridviewMain2_KeyDown);
            this.ux_datagridviewSheet.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ux_datagridviewSheet_MouseUp);
            // 
            // ux_groupboxSheet
            // 
            this.ux_groupboxSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxSheet.Controls.Add(this.ux_textBoxSep2);
            this.ux_groupboxSheet.Controls.Add(this.ux_textBoxSep1);
            this.ux_groupboxSheet.Controls.Add(this.label8);
            this.ux_groupboxSheet.Controls.Add(this.ux_comboboxMethod);
            this.ux_groupboxSheet.Controls.Add(this.ux_datagridviewSheet);
            this.ux_groupboxSheet.Controls.Add(this.ux_checkBoxShowOnlyRowsWithError);
            this.ux_groupboxSheet.Controls.Add(this.ux_checkboxIsArchived);
            this.ux_groupboxSheet.Controls.Add(this.label1);
            this.ux_groupboxSheet.Controls.Add(this.label7);
            this.ux_groupboxSheet.Controls.Add(this.label2);
            this.ux_groupboxSheet.Controls.Add(this.label3);
            this.ux_groupboxSheet.Location = new System.Drawing.Point(13, 208);
            this.ux_groupboxSheet.Name = "ux_groupboxSheet";
            this.ux_groupboxSheet.Size = new System.Drawing.Size(903, 360);
            this.ux_groupboxSheet.TabIndex = 18;
            this.ux_groupboxSheet.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(463, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Accession Number Formula : accession_part1 + ___ + accession_part2 + ___ + access" +
    "ion_part3";
            // 
            // ux_comboboxMethod
            // 
            this.ux_comboboxMethod.FormattingEnabled = true;
            this.ux_comboboxMethod.Location = new System.Drawing.Point(640, 17);
            this.ux_comboboxMethod.Name = "ux_comboboxMethod";
            this.ux_comboboxMethod.Size = new System.Drawing.Size(163, 21);
            this.ux_comboboxMethod.TabIndex = 24;
            this.ux_comboboxMethod.SelectedIndexChanged += new System.EventHandler(this.ux_comboboxMethod_SelectedIndexChanged);
            // 
            // ux_checkBoxShowOnlyRowsWithError
            // 
            this.ux_checkBoxShowOnlyRowsWithError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_checkBoxShowOnlyRowsWithError.AutoSize = true;
            this.ux_checkBoxShowOnlyRowsWithError.Location = new System.Drawing.Point(6, 339);
            this.ux_checkBoxShowOnlyRowsWithError.Name = "ux_checkBoxShowOnlyRowsWithError";
            this.ux_checkBoxShowOnlyRowsWithError.Size = new System.Drawing.Size(146, 17);
            this.ux_checkBoxShowOnlyRowsWithError.TabIndex = 0;
            this.ux_checkBoxShowOnlyRowsWithError.Text = "Show only rows with error";
            this.ux_checkBoxShowOnlyRowsWithError.UseVisualStyleBackColor = true;
            this.ux_checkBoxShowOnlyRowsWithError.CheckedChanged += new System.EventHandler(this.ux_checkBoxShowOnlyRowsWithError_CheckedChanged);
            // 
            // ux_checkboxIsArchived
            // 
            this.ux_checkboxIsArchived.AutoSize = true;
            this.ux_checkboxIsArchived.Location = new System.Drawing.Point(818, 19);
            this.ux_checkboxIsArchived.Name = "ux_checkboxIsArchived";
            this.ux_checkboxIsArchived.Size = new System.Drawing.Size(79, 17);
            this.ux_checkboxIsArchived.TabIndex = 23;
            this.ux_checkboxIsArchived.Text = "Is Archived";
            this.ux_checkboxIsArchived.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(585, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Method :";
            // 
            // contextMenuStripMenu
            // 
            this.contextMenuStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyHeadersToolStripMenuItem,
            this.updateLookupTablesToolStripMenuItem});
            this.contextMenuStripMenu.Name = "contextMenuStripMenu";
            this.contextMenuStripMenu.Size = new System.Drawing.Size(187, 48);
            // 
            // copyHeadersToolStripMenuItem
            // 
            this.copyHeadersToolStripMenuItem.Name = "copyHeadersToolStripMenuItem";
            this.copyHeadersToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.copyHeadersToolStripMenuItem.Text = "Copy headers";
            this.copyHeadersToolStripMenuItem.Click += new System.EventHandler(this.copyHeadersToolStripMenuItem_Click);
            // 
            // updateLookupTablesToolStripMenuItem
            // 
            this.updateLookupTablesToolStripMenuItem.Name = "updateLookupTablesToolStripMenuItem";
            this.updateLookupTablesToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.updateLookupTablesToolStripMenuItem.Text = "Update lookup tables";
            this.updateLookupTablesToolStripMenuItem.Click += new System.EventHandler(this.updateLookupTablesToolStripMenuItem_Click);
            // 
            // ux_textBoxSep1
            // 
            this.ux_textBoxSep1.Location = new System.Drawing.Point(236, 16);
            this.ux_textBoxSep1.Name = "ux_textBoxSep1";
            this.ux_textBoxSep1.Size = new System.Drawing.Size(20, 20);
            this.ux_textBoxSep1.TabIndex = 28;
            this.ux_textBoxSep1.Text = " ";
            // 
            // ux_textBoxSep2
            // 
            this.ux_textBoxSep2.Location = new System.Drawing.Point(356, 16);
            this.ux_textBoxSep2.Name = "ux_textBoxSep2";
            this.ux_textBoxSep2.Size = new System.Drawing.Size(20, 20);
            this.ux_textBoxSep2.TabIndex = 29;
            this.ux_textBoxSep2.Text = ".";
            // 
            // BatchWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 614);
            this.Controls.Add(this.ux_groupboxSheet);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ux_groupBox_Descriptors);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BatchWizard";
            this.Text = "Batch Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AccessionBatchWizard_FormClosing);
            this.Load += new System.EventHandler(this.AccessionBatchWizard_Load);
            this.ux_groupBox_Descriptors.ResumeLayout(false);
            this.ux_groupBox_Descriptors.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewSheet)).EndInit();
            this.ux_groupboxSheet.ResumeLayout(false);
            this.ux_groupboxSheet.PerformLayout();
            this.contextMenuStripMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ux_Button_Close;
        private System.Windows.Forms.Button ux_Button_Add_Descriptor;
        private System.Windows.Forms.ListBox ux_listBoxCategory;
        private System.Windows.Forms.ListBox ux_listBoxTrait;
        private System.Windows.Forms.GroupBox ux_groupBox_Descriptors;
        private System.Windows.Forms.Button ux_buttonRemoveDescriptor;
        private System.Windows.Forms.ListBox ux_listBoxDescriptors;
        private System.Windows.Forms.ListBox ux_listBoxCrop;
        private System.Windows.Forms.ProgressBar ux_progressBarWorking;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ux_buttonSaveData;
        private System.Windows.Forms.DataGridView ux_datagridviewSheet;
        private System.Windows.Forms.Label ux_labelMessage;
        private System.Windows.Forms.Button ux_buttonTemplate;
        private System.Windows.Forms.Button ux_buttonClear;
        private System.Windows.Forms.GroupBox ux_groupboxSheet;
        private System.Windows.Forms.CheckBox ux_checkBoxShowOnlyRowsWithError;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox ux_checkboxIsArchived;
        private System.Windows.Forms.ComboBox ux_comboboxMethod;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripMenu;
        private System.Windows.Forms.ToolStripMenuItem copyHeadersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateLookupTablesToolStripMenuItem;
        private System.Windows.Forms.TextBox ux_textBoxSep2;
        private System.Windows.Forms.TextBox ux_textBoxSep1;
    }
}