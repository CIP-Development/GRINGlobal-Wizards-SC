﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace AccessionBatchWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class BatchWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;
        DataSet _changedRecords = new DataSet();
        
        Dictionary<string, string> _descriptors = new Dictionary<string, string>();
        BindingList<KeyValuePair<string, string>> _descriptors2 = new BindingList<KeyValuePair<string, string>>();
        
        DataTable _main2;

        string crop_id = string.Empty;
        string _method_id = "0";
        DataTable dtPassportCriteria;

        int startDescriptorsIndex = 5; //En caso que hubieran otras columnas fijas aparte de "02. Accession Number" como accession_id o inventory_id
        private class GroupColumn {
            public string[] RequiredColumns { set; get; }
            public bool Processed { set; get; }
            public GroupColumn(string[] requiredColumns) { RequiredColumns = requiredColumns; Processed = false; }
        }
        Dictionary<string, GroupColumn> GroupColumnMan = new Dictionary<string, GroupColumn>();
        
        public string FormName
        {
            get
            {
                return "Batch Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "accession_id";
            }
        }

        public BatchWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();
            _sharedUtils = sharedUtils;
        }

        private void AccessionBatchWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void ux_Button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AccessionBatchWizard_Load(object sender, EventArgs e)
        {
            /**/
            _main2 = new DataTable();
            _main2.Columns.Add("02. Accession number");
            _main2.Columns.Add("accession_id");
            _main2.Columns.Add("inventory_id");
            _main2.Columns.Add("taxonomy_species_name");
            _main2.Columns.Add("source_type_code");
            _main2.Columns["taxonomy_species_name"].Caption = "Taxon name";
            _main2.Columns["source_type_code"].Caption = "Source type code";
            
            ux_datagridviewSheet.DataSource = _main2;
            ux_datagridviewSheet.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            ux_datagridviewSheet.Columns["taxonomy_species_name"].HeaderText = "Taxon name";
            ux_datagridviewSheet.Columns["source_type_code"].HeaderText = "Source type code";
            /**/

            //Load passport datatable
            DataSet passportCriteria = _sharedUtils.GetWebServiceData("get_passport_columns", "", 0, 0);
            if (passportCriteria != null && passportCriteria.Tables.Contains("get_passport_columns"))
            {
                dtPassportCriteria = passportCriteria.Tables["get_passport_columns"];
            }
            
            // Populate the dropdown list of 'Crop names'
            DataTable crop  = _sharedUtils.GetLocalData("SELECT value_member, display_member FROM crop_lookup", "");
            if (crop.Rows.Count > 0)
            {
                crop.DefaultView.Sort = "display_member ASC";
                ux_listBoxCrop.ValueMember = "value_member";
                ux_listBoxCrop.DisplayMember = "display_member";
                ux_listBoxCrop.DataSource = crop;
            }

            GroupColumnMan.Add("accession_source_geography_id", new GroupColumn(new string[] { "13. Country of origin", "Adm1", "Adm2", "Adm3" }));

            //Load method
            DataSet dsMethod = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_method", "", 0, 0);
            if (dsMethod != null && dsMethod.Tables.Contains("acc_batch_wizard_get_method"))
            {
                ux_comboboxMethod.DataSource = dsMethod.Tables["acc_batch_wizard_get_method"];
                ux_comboboxMethod.ValueMember = "method_id";
                ux_comboboxMethod.DisplayMember = "name";
            }
        }

        private void ux_listBoxCrop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ux_listBoxCrop.DataSource == null) return;

            _descriptors.Clear();
            ux_listBoxDescriptors.DataSource = null;

            for (int i = _main2.Columns.Count - 1; i >= startDescriptorsIndex ; i--) {
                _main2.Columns.RemoveAt(i);
            }
            _main2.Rows.Clear();
            
            crop_id = ux_listBoxCrop.SelectedValue.ToString();

            string query = "select distinct category, b.display_member from cip_crop_trait_lookup a join code_value_lookup b on a.category = b.value_member and b.group_name = 'DESCRIPTOR_CATEGORY' where crop_id = ";
            DataTable localDBCodeCategoryTable = _sharedUtils.GetLocalData(query + ux_listBoxCrop.SelectedValue.ToString(), "");
            
            DataRow nc = localDBCodeCategoryTable.NewRow();
            nc["category"] = "PASSPORT";
            nc["display_member"] = "Passport Descriptors";
            localDBCodeCategoryTable.Rows.Add(nc);

            if (localDBCodeCategoryTable.Rows.Count > 0)
            {
                ux_listBoxCategory.ValueMember = "category";
                ux_listBoxCategory.DisplayMember = "display_member";
                ux_listBoxCategory.DataSource = localDBCodeCategoryTable.DefaultView;
            }
            
        }

        private void ux_listBoxCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ux_listBoxTrait.DataSource = null;
            ux_listBoxTrait.DataBindings.Clear();

            if (ux_listBoxCategory.SelectedValue.ToString().Equals("PASSPORT"))
            {
                if (dtPassportCriteria != null)
                {
                    ux_listBoxTrait.ValueMember = "value_member";
                    ux_listBoxTrait.DisplayMember = "display_member";
                    ux_listBoxTrait.DataSource = dtPassportCriteria.DefaultView;
                }
            }
            else
            {
                DataTable localDBTraitTable = _sharedUtils.GetLocalData("select * from cip_crop_trait_lookup where crop_id = " + ux_listBoxCrop.SelectedValue.ToString() + " and category = '" + ux_listBoxCategory.SelectedValue.ToString() + "'", "");
                if (localDBTraitTable.Rows.Count > 0)
                {
                    localDBTraitTable.DefaultView.Sort = "display_member ASC";
                    ux_listBoxTrait.ValueMember = "value_member";
                    ux_listBoxTrait.DisplayMember = "display_member";
                    ux_listBoxTrait.DataSource = localDBTraitTable.DefaultView;
                }
            }
        }

        private void ux_Button_Add_Descriptor_Click(object sender, EventArgs e)
        {
            if (ux_listBoxTrait.SelectedItem == null) return;

            if (ux_listBoxCategory.SelectedValue.ToString().Equals("PASSPORT"))
            {
                if (!_descriptors.ContainsKey(ux_listBoxTrait.SelectedValue.ToString()))
                {
                    string selectedValue = ux_listBoxTrait.SelectedValue.ToString();
                    _descriptors.Add(selectedValue, ux_listBoxTrait.Text);

                    if (GroupColumnMan.ContainsKey(selectedValue)) {
                        foreach (var column_name in GroupColumnMan[selectedValue].RequiredColumns)
                        {
                            _main2.Columns.Add(column_name);
                            _main2.Columns[column_name].Caption = column_name;
                            ux_datagridviewSheet.Columns[column_name].Tag = new ColumnTag("GROUP_TRAIT");
                        }
                    }
                    else
                    {
                        _main2.Columns.Add(selectedValue);
                        _main2.Columns[selectedValue].Caption = ux_listBoxTrait.Text;
                        ux_datagridviewSheet.Columns[selectedValue].HeaderText = ux_listBoxTrait.Text;

                        ux_datagridviewSheet.Columns[selectedValue].Tag = new ColumnTag("PASSPORT");
                    }

                    ux_listBoxDescriptors.DataSource = null;
                    ux_listBoxDescriptors.DataSource = new BindingSource(_descriptors, null);
                    ux_listBoxDescriptors.ValueMember = "Key";
                    ux_listBoxDescriptors.DisplayMember = "Value";
                }
            }
            else
            {
                if (!_descriptors.ContainsKey(ux_listBoxTrait.SelectedValue.ToString()))
                {
                    _descriptors.Add(ux_listBoxTrait.SelectedValue.ToString(), ux_listBoxTrait.Text);

                    ux_listBoxDescriptors.DataSource = null;
                    ux_listBoxDescriptors.DataSource = new BindingSource(_descriptors, null);
                    ux_listBoxDescriptors.ValueMember = "Key";
                    ux_listBoxDescriptors.DisplayMember = "Value";
                    
                    DataRowView desc = (DataRowView)ux_listBoxTrait.SelectedItem;
                    
                    /**/
                    _main2.Columns.Add(ux_listBoxTrait.SelectedValue.ToString());
                    _main2.Columns[ux_listBoxTrait.SelectedValue.ToString()].Caption = ux_listBoxTrait.Text;
                    ux_datagridviewSheet.Columns[ux_listBoxTrait.SelectedValue.ToString()].HeaderText = ux_listBoxTrait.Text;

                    ux_datagridviewSheet.Columns[ux_listBoxTrait.SelectedValue.ToString()].Tag = new ColumnTag("CROP_TRAIT");
                    //ux_datagridviewMain2.Columns[ux_listBoxTrait.SelectedValue.ToString()].Tag = desc.Row.Field<string>("type");
                    
                }
            }
        }

        private void ux_buttonRemoveDescriptor_Click(object sender, EventArgs e)
        {
            if (ux_listBoxDescriptors.SelectedItem == null) return;

            string key = ux_listBoxDescriptors.SelectedValue.ToString();
            if (_descriptors.Remove(key)) {
                ux_listBoxDescriptors.DataSource = null;
                
                if(_descriptors.Count > 0) { 
                    ux_listBoxDescriptors.DataSource = new BindingSource(_descriptors, null);
                    ux_listBoxDescriptors.ValueMember = "Key";
                    ux_listBoxDescriptors.DisplayMember = "Value";
                }

                if (GroupColumnMan.ContainsKey(key))
                {
                    foreach (var column_name in GroupColumnMan[key].RequiredColumns)
                    {
                        _main2.Columns.Remove(column_name);
                    }
                }
                else
                    _main2.Columns.Remove(key);
                
            }
        }
        
        public bool ProcessDGVEditShortcutKeys(DataGridView dgv, KeyEventArgs e, string cno, LookupTables lookupTables) {
            bool keyProcessed = false;

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            if (e.KeyCode == Keys.V && e.Control)
            {
                IDataObject dataObj = Clipboard.GetDataObject();
                string pasteText = "";
                
                if (dataObj.GetDataPresent(System.Windows.Forms.DataFormats.UnicodeText))
                {
                    char[] rowDelimiters = new char[] { '\r', '\n' };
                    char[] columnDelimiters = new char[] { '\t' };
                    int badRows = 0;
                    int missingRows = 0;
                    bool importSuccess = false;

                    // Processing keystroke...
                    keyProcessed = true;

                    pasteText = dataObj.GetData(DataFormats.UnicodeText).ToString();
                    
                    DataTable dt = (DataTable)dgv.DataSource;
                    importSuccess = ImportTextToDataTableUsingKeys(pasteText, dt, rowDelimiters, columnDelimiters, out badRows, out missingRows, lookupTables);
                    if (!importSuccess)
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("An error has been ocurred", "Error", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
                        ggMessageBox.ShowDialog();
                    }
                    
                }
            }

            if (e.KeyCode == Keys.C && e.Control)
            {
                StringBuilder copyString = new StringBuilder();
                // First we need to get the min/max rows and columns for the selected cells...
                int minCol = dgv.Columns.Count;
                int maxCol = -1;
                int minRow = dgv.Rows.Count;
                int maxRow = -1;

                // Processing keystroke...
                keyProcessed = true;

                foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                {
                    if (dgvc.ColumnIndex < minCol) minCol = dgvc.ColumnIndex;
                    if (dgvc.ColumnIndex > maxCol) maxCol = dgvc.ColumnIndex;
                    if (dgvc.RowIndex < minRow) minRow = dgvc.RowIndex;
                    if (dgvc.RowIndex > maxRow) maxRow = dgvc.RowIndex;
                }

                // First, gather the column headers (but only if the entire row was selected)...
                if (dgv.SelectedRows.Count != 0)
                {
                    for (int i = minCol; i <= maxCol; i++)
                    {
                        copyString.Append(dgv.Columns[i].HeaderText);
                        if(i == maxCol)
                            copyString.Append("\r\n");
                        else
                            copyString.Append("\t");
                    }
                }
                // Now build the string to pass to the clipboard...
                for (int i = minRow; i <= maxRow; i++)
                {
                    for (int j = minCol; j <= maxCol; j++)
                    {
                        switch (dgv[j, i].FormattedValueType.Name)
                        {
                            case "Boolean":
                                copyString.Append(dgv[j, i].Value.ToString());
                                break;
                            default:
                                if (dgv[j, i].FormattedValue == null || dgv[j, i].FormattedValue.ToString().ToLower() == "[null]")
                                {
                                    copyString.Append("");
                                }
                                else
                                {
                                    copyString.Append( dgv[j, i].FormattedValue.ToString());
                                }
                                break;
                        }

                        if (j == maxCol) {
                            if(i < maxRow)
                                copyString.Append("\r\n");
                        }
                        else
                            copyString.Append("\t");
                    }
                }

                // Pass the new string to the clipboard...
                Clipboard.SetDataObject(copyString, false, 1, 1000);
                
            }

            if (e.KeyCode == Keys.Delete)
            {
                // Processing keystroke...
                keyProcessed = true;

                if (dgv.SelectedRows.Count == 0)
                {
                    // The user is deleting values from individual selected cells (not entire rows)...
                    foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                    {
                        DataRowView drv = (DataRowView)dgvc.OwningRow.DataBoundItem;
                        if (drv == null) //if (dgv.Rows[row].IsNewRow)
                        {
                            dgvc.Value = "";
                            dgv.UpdateCellValue(dgvc.ColumnIndex, dgvc.RowIndex);
                        }
                        else
                        {
                            if (!drv[dgvc.OwningColumn.Index].Equals(DBNull.Value))
                            {
                                if (!dgvc.ReadOnly)
                                {
                                    // Edit the DataRow (not the DataRowView) so that row state is changed...
                                    drv.Row[dgvc.OwningColumn.Index] = DBNull.Value;
                                    // For unbound text cells we have to manually clear the cell's text...
                                    if (string.IsNullOrEmpty(dgvc.OwningColumn.DataPropertyName)) dgvc.Value = "";
                                    dgv.UpdateCellValue(dgvc.ColumnIndex, dgvc.RowIndex);
                                }
                            }
                        }
                    }
                }
                else
                {
                    // The user is attempting to delete entire rows from the datagridview...
                    
                    
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You are about to delete {0} records!\n\nAre you sure you want to do this?", "Record Delete Confirmation", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
                    ggMessageBox.Name = "UserInterfaceUtils_ProcessDGVEditShortcutKeysMessage1";
                    
                    string[] argsArray = new string[100];
                    argsArray[0] = dgv.SelectedRows.Count.ToString();
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    
                    if (DialogResult.OK == ggMessageBox.ShowDialog())
                    {
                        foreach (DataGridViewRow dgvr in dgv.SelectedRows)
                        {
                            if(!dgvr.IsNewRow)
                                dgv.Rows.Remove(dgvr);
                        }
                    }
                    e.Handled = true;
                }
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;

            return keyProcessed;
        }

        public bool ImportTextToDataTableUsingKeys(string rawImportText, DataTable destinationTable, char[] rowDelimiters, char[] columnDelimiters, out int badRows, out int missingRows, LookupTables lookupTables)
        {
            
            string scrubbedRawImportText = "";

            // Attempting to remove new lines with no matching carriage return (ex. \n with no leading \r) and vice versa (ex. \r with no trailing \n)...
            // First protect the well-formed carriage return line feed (\r\n) by temp. removing it and substituting a placeholder (to aid in weeding out lone \n and \r)...
            scrubbedRawImportText = rawImportText.Replace("\r\n", "***well-formed carriage return line feed***").Replace("\n\r", "***well-formed carriage return line feed***");
            // Now remove any remaining lone \n or \r that cannot be processed properly...
            
            // Next return the well-formed carriage return line feeds back where they belong...
            scrubbedRawImportText = scrubbedRawImportText.Replace("***well-formed carriage return line feed***", "\r\n");
            
            string[] rawImportRows = scrubbedRawImportText.Split(rowDelimiters, StringSplitOptions.RemoveEmptyEntries);
            
            bool primaryKeyFound = false;
            bool processedImportSuccessfully = true;
            int primaryKeyIndex = -1;
            badRows = 0;
            missingRows = 0;
            
            if (rawImportRows == null || rawImportRows.Length <= 0) return false;

            try
            {
                string[] importColumnNames = rawImportRows[0].Split(columnDelimiters, StringSplitOptions.None);
                System.Collections.Generic.Dictionary<int, int> importedColumns = new System.Collections.Generic.Dictionary<int, int>();

                for (int i = 0; i < importColumnNames.Length; i++)
                {
                    if (!primaryKeyFound && importColumnNames[i].ToUpper().Equals("02. ACCESSION NUMBER"))
                    {
                        primaryKeyFound = true;
                        primaryKeyIndex = i;
                        importedColumns.Add(i, 0);
                        continue;
                    }

                    for (int j = 1; j < destinationTable.Columns.Count; j++)
                    {
                        if (importColumnNames[i].ToUpper().Trim().Equals(destinationTable.Columns[j].Caption.ToUpper().Trim()))
                        {
                            importedColumns.Add(i, j);
                            break;
                        }
                    }
                }

                for (int iRow = 1; iRow < rawImportRows.Length; iRow++)
                {
                    DataRow toInsert;

                    string[] sourceRow = rawImportRows[iRow].Split(columnDelimiters, StringSplitOptions.None);


                    DataRow[] Rows = destinationTable.Select("[02. Accession number]='" + sourceRow[primaryKeyIndex] + "'");
                    if (!Rows.Any())
                    {
                        toInsert = destinationTable.NewRow();
                    }
                    else
                    {
                        toInsert = Rows[0];
                    }

                    foreach (var col in importedColumns)
                    {
                        toInsert[col.Value] = sourceRow[col.Key];
                    }

                    if (!Rows.Any())
                        destinationTable.Rows.Add(toInsert);
                }
            }
            catch (Exception e) {
                processedImportSuccessfully = false;
            }
            
            return processedImportSuccessfully;
        }

        private void ux_listBoxTrait_DoubleClick(object sender, EventArgs e)
        {
            ux_Button_Add_Descriptor_Click(null, null);
        }
        
        private void ux_buttonSaveData_Click(object sender, EventArgs e)
        {
            //ux_checkBoxShowOnlyRowsWithError.Checked = false;

            ux_labelMessage.Text = string.Empty;
            ux_buttonSaveData.Enabled = false;
            ux_progressBarWorking.Visible = true;
            ux_progressBarWorking.Style = ProgressBarStyle.Marquee;
            System.Threading.Thread thread =
              new System.Threading.Thread(new System.Threading.ThreadStart(saveData));
            thread.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accnumb"></param>
        /// <returns></returns>
        /// <exception cref="Exception">If accnumb doesnt match regular expression.</exception>
        private string getAccessionNumber(string accnumb) {
            
            Regex rgx = new Regex(@"^[a-zA-Z0-9]+ [0-9]+( [A-Za-z0-9]*)?$");
            if (! rgx.IsMatch(accnumb))
                throw new Exception("Regular expression is not matched");

            StringBuilder sb = new StringBuilder();
            int index1 = accnumb.IndexOf(" ");
            if (index1 > -1)
            {
                sb.Append(accnumb.Substring(0, index1 + 1));
                int index2 = accnumb.IndexOf(" ", index1 + 1);
                if (index2 > -1)
                {
                    int part2 = int.Parse(accnumb.Substring(index1 + 1, index2 - (index1 + 1)));
                    sb.Append(part2);
                    if (accnumb.Length > index2 + 1)
                    {
                        sb.Append(" " + accnumb.Substring(index2 + 1, accnumb.Length - (index2 + 1)));
                    }
                    else
                        throw new Exception("part3 is not found");
                }
                else
                {
                    if (accnumb.Length > index1 + 1)
                    {
                        int part2 = int.Parse(accnumb.Substring(index1 + 1, accnumb.Length - (index1 + 1)));
                        sb.Append(part2);
                    }
                    else
                    {
                        throw new Exception("part 2 is not found");
                    }
                }
            }
            else
                throw new Exception("1st separator is not found");
            
            return sb.ToString();
        }

        public void saveData()
        {
            try
            {
                DataGridView datagridviewContainer = ux_datagridviewSheet;
                DataTable dtSourceContainer = _main2;
                var watch = System.Diagnostics.Stopwatch.StartNew();
                
                List<string> AccessionNumbers = new List<string>();
                List<int> crop_trait_ids = new List<int>();
                //Obtain all crop_trait_ids from columns
                foreach (DataGridViewColumn col in datagridviewContainer.Columns)
                {
                    if (col.Index < startDescriptorsIndex) continue;
                    int crop_trait_id = -1;
                    int.TryParse(col.Name, out crop_trait_id);
                    if (crop_trait_id != -1)
                        crop_trait_ids.Add(crop_trait_id);
                }
                //Get all cooperator organizations
                DataSet dsCooperatorOrganization = null;
                DataTable dtCooperatorOrganization = null;
                if (dtSourceContainer.Columns.Contains("18.1"))
                {
                    dsCooperatorOrganization = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_cooperator_org", "", 0, 0);
                    if (dsCooperatorOrganization != null && dsCooperatorOrganization.Tables.Contains("acc_batch_wizard_get_cooperator_org"))
                        dtCooperatorOrganization = dsCooperatorOrganization.Tables["acc_batch_wizard_get_cooperator_org"];
                }
                //Fix some values depend on column name
                foreach (DataGridViewRow row in datagridviewContainer.Rows)
                {
                    if (row.Cells[0].Value == null || row.IsNewRow) continue;
                    if (string.IsNullOrEmpty(row.Cells[0].Value.ToString().Trim())) continue;
                    // Add cipnumbers to check if exist

                    row.Cells[0].Value = row.Cells[0].Value.ToString().Trim();
                    AccessionNumbers.Add("'" + row.Cells[0].Value.ToString() + "'");
                    
                    if (dtSourceContainer.Columns.Contains("20.3")) {
                        AccessionNumbers.Add("'" + row.Cells["20.3"].Value.ToString() + "'");
                    }
                    if (dtSourceContainer.Columns.Contains("20.4")) {
                        AccessionNumbers.Add("'" + row.Cells["20.4"].Value.ToString() + "'");
                    }
                    // Create cooperator organization if not exist
                    if (dtSourceContainer.Columns.Contains("18.1") && !string.IsNullOrEmpty(row.Cells["18.1"].Value.ToString()))
                    {
                        if (dtCooperatorOrganization != null)
                        {
                            var rows = dtCooperatorOrganization.Select("organization = '" + row.Cells["18.1"].Value + "'");
                            if (rows != null && !rows.Any())
                            {// insert cooperator
                                DataRow d = dtCooperatorOrganization.NewRow();
                                d["organization"] = row.Cells["18.1"].Value.ToString();
                                d["status_code"] = "ACTIVE";
                                d["sys_lang_id"] = _sharedUtils.UserLanguageCode;
                                dtCooperatorOrganization.Rows.Add(d);
                            }
                        }
                    }
                }
                //Save cooperator changes
                _sharedUtils.SaveWebServiceData(dsCooperatorOrganization);
                // kz-pending usar dsError en vez de llamar denuevo a acc_batch_wizard_get_cooperator_org
                //Get all cooperator organization again
                dsCooperatorOrganization = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_cooperator_org", "", 0, 0);
                if (dsCooperatorOrganization != null && dsCooperatorOrganization.Tables.Contains("acc_batch_wizard_get_cooperator_org"))
                    dtCooperatorOrganization = dsCooperatorOrganization.Tables["acc_batch_wizard_get_cooperator_org"];
                

                if (AccessionNumbers.Count > 0)
                {  //Get all accession_ids and inventory_ids
                    string sql = @"select a.display_member as AccNumber, a.value_member as accession_id, i.value_member as inventory_id
from accession_lookup a join inventory_lookup i on i.accession_id = a.value_member
where a.display_member in (" + string.Join(",", AccessionNumbers.ToArray()) + ")";

                    DataTable dtAccessionInventoryLookup = _sharedUtils.GetLocalData(sql, "");
                    
                    List<int> inventory_ids = new List<int>();
                    foreach (DataRow r in dtAccessionInventoryLookup.Rows)
                        inventory_ids.Add(r.Field<int>("inventory_id"));

                    string[] string_inventory_ids = inventory_ids.ConvertAll(x => x.ToString()).ToArray();
                    string[] string_crop_trait_ids = crop_trait_ids.ConvertAll(x => x.ToString()).ToArray();

                    //Get all observations by method_id
                    string parameters = ":inventoryid=" + string.Join(",", string_inventory_ids) + ";:croptraitid=" + string.Join(",", string_crop_trait_ids) + ";:methodid=" + _method_id;
                    string get_crop_trait_observation_update = "accession_batch_wizard_get_crop_trait_observation";
                    DataSet dsObservationUpdate = _sharedUtils.GetWebServiceData(get_crop_trait_observation_update, parameters, 0, 0);
                    DataTable dtObservationUpdate = null;
                    if (dsObservationUpdate != null && dsObservationUpdate.Tables.Contains(get_crop_trait_observation_update))
                    {
                        dtObservationUpdate = dsObservationUpdate.Tables[get_crop_trait_observation_update];
                    }
                    DataSet dsCropTraitIsCoded = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_crop_trait_type", ":croptraitid=" + string.Join(",", string_crop_trait_ids), 0, 0);
                    DataTable dtCropTraitIsCoded = null;
                    if (dsCropTraitIsCoded != null && dsCropTraitIsCoded.Tables.Contains("acc_batch_wizard_get_crop_trait_type"))
                    {
                        dtCropTraitIsCoded = dsCropTraitIsCoded.Tables["acc_batch_wizard_get_crop_trait_type"];
                    }

                    DataSet dsAccession = _sharedUtils.GetWebServiceData("accession_batch_wizard_get_accession", ":accessionid=-1", 0, 0);
                    DataTable newAccessions = null;    
                    List<DataGridViewRow> dgvrowsNewAccessions = new List<DataGridViewRow>();
                    if (dsAccession != null && dsAccession.Tables["accession_batch_wizard_get_accession"] != null)
                        newAccessions = dsAccession.Tables["accession_batch_wizard_get_accession"];

                    DataTable dtTableUpdate = new DataTable();
                    dtTableUpdate.Columns.Add("table_name", typeof(string));
                    dtTableUpdate.Columns.Add("table_id", typeof(Int32));
                    dtTableUpdate.Columns.Add("filter_column", typeof(string));
                    dtTableUpdate.Columns.Add("filter_value", typeof(string));
                    dtTableUpdate.Columns.Add("column_name", typeof(string));
                    dtTableUpdate.Columns.Add("column_type", typeof(string));
                    dtTableUpdate.Columns.Add("int_value", typeof(Int32));
                    dtTableUpdate.Columns.Add("string_value", typeof(string));
                    dtTableUpdate.Columns.Add("decimal_value", typeof(Decimal));
                    dtTableUpdate.Columns.Add("datetime_value", typeof(DateTime));
                    dtTableUpdate.Columns.Add("cell", typeof(DataGridViewCell));
                    dtTableUpdate.Columns.Add("second_table_id", typeof(Int32));

                    //Validate CIPNumbers
                    foreach (DataGridViewRow row in datagridviewContainer.Rows)
                    {
                        if (row.IsNewRow) continue;

                        row.ErrorText = string.Empty;
                        if (row.Cells[0].Value == null)
                        {
                            row.ErrorText = "Accession Number is blank";
                            continue;
                        }

                        if (string.IsNullOrEmpty(row.Cells[0].Value.ToString().Trim()))
                        {
                            row.ErrorText = "Accession Number is blank";
                            continue;
                        }

                        string ACCNumber = row.Cells[0].Value.ToString().Trim();
                        
                        var resp = dtAccessionInventoryLookup.Select("AccNumber = '" + ACCNumber + "'");
                        if (resp == null || !resp.Any()) //Create Accession
                        {
                            #region create_accession
                            DataTable dtTaxonomySpecies = null;
                            if (row.Cells["taxonomy_species_name"].Value == null)
                                row.ErrorText = "taxonomy_species_name not found";
                            else
                            {
                                var sqltsl = "select value_member from taxonomy_species_lookup where display_member = '" + row.Cells["taxonomy_species_name"].Value + "'";
                                dtTaxonomySpecies = _sharedUtils.GetLocalData(sqltsl, "");
                            }
                            if (dtTaxonomySpecies != null && dtTaxonomySpecies.Rows.Count == 1)
                            {
                                DataRow dr = newAccessions.NewRow();
                                string[] accNumberParts = ACCNumber.Split(' ');

                                dr["accession_number_part1"] = accNumberParts[0];
                                dr["accession_number_part2"] = accNumberParts.Length > 1 ? accNumberParts[1] : "";
                                dr["accession_number_part3"] = accNumberParts.Length > 2 ? accNumberParts[2] : "";
                                dr["is_backed_up"] = "N";
                                dr["is_core"] = "N";
                                dr["is_web_visible"] = "N";
                                dr["status_code"] = "Not Defined";
                                dr["taxonomy_species_id"] = dtTaxonomySpecies.Rows[0].Field<int>("value_member");
                                string sNow = DateTime.Now.ToString();
                                dr["created_by"] = _sharedUtils.UserCooperatorID;
                                dr["created_date"] = sNow;
                                dr["owned_by"] = _sharedUtils.UserCooperatorID;
                                dr["owned_date"] = sNow;

                                newAccessions.Rows.Add(dr);
                                dgvrowsNewAccessions.Add(row);
                            }
                            else if (dtTaxonomySpecies.Rows.Count == 0)
                            {
                                row.ErrorText = "Species not found";
                            }
                            else if (dtTaxonomySpecies.Rows.Count > 1)
                                row.ErrorText = "More than one species found";
                            else
                                row.ErrorText = "There was an error searching taxonomy species id";
                            #endregion
                        }
                        else
                        {
                            #region accession_exists
                            //copy accession_id and inventory_id
                            row.Cells["accession_id"].Value = resp[0].Field<int>("accession_id");
                            row.Cells["inventory_id"].Value = resp[0].Field<int>("inventory_id");

                            //check if taxonomy_species_name is changed
                            DataTable dtTaxonomySpecies = null;
                            row.Cells["taxonomy_species_name"].Style.BackColor = Color.White;

                            if (row.Cells["taxonomy_species_name"].Value != null && !string.IsNullOrEmpty(row.Cells["taxonomy_species_name"].Value.ToString()))
                            {
                                var sqltsl = "select value_member from taxonomy_species_lookup where display_member = '" + row.Cells["taxonomy_species_name"].Value + "'";
                                dtTaxonomySpecies = _sharedUtils.GetLocalData(sqltsl, "");

                                if (dtTaxonomySpecies != null && dtTaxonomySpecies.Rows.Count == 1)
                                {
                                    //insert table update
                                    DataRow dr = dtTableUpdate.NewRow();
                                    dr["table_name"] = "accession";
                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                    dr["column_name"] = "taxonomy_species_id";
                                    dr["column_type"] = "int";
                                    dr["int_value"] = dtTaxonomySpecies.Rows[0].Field<int>("value_member");
                                    dr["cell"] = row.Cells["taxonomy_species_name"];
                                    dtTableUpdate.Rows.Add(dr);
                                    //row.Cells["taxonomy_species_name"].Style.BackColor = Color.Orange;
                                }
                                else if (dtTaxonomySpecies.Rows.Count == 0)
                                {
                                    row.ErrorText = "Species not found";
                                }
                                else if (dtTaxonomySpecies.Rows.Count > 1)
                                    row.ErrorText = "More than one species found";
                                else
                                    row.ErrorText = "There was an error searching taxonomy species id";
                            }
                            #endregion
                        }
                    }
                    //check if necessary to create accessions
                    #region save_accessions
                    if (newAccessions.Rows.Count > 0)
                    {
                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsAccession);
                        if (saveErrors != null && saveErrors.Tables.Contains("accession_batch_wizard_get_accession"))
                        {
                            foreach (DataRow dr in saveErrors.Tables["accession_batch_wizard_get_accession"].Rows)
                            {
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[0].Value.ToString().Equals("CIP " + dr["cipnumber"].ToString())).First();
                                    if (row != null) row.ErrorText = dr["ExceptionMessage"].ToString();
                                    MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                        , dr["cipnumber"].ToString(), dr["ExceptionMessage"].ToString()));
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                        , dr["cipnumber"].ToString(), dr["ExceptionMessage"].ToString()));
                                }
                                else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                        , dr["cipnumber"].ToString(), dr["ExceptionMessage"].ToString()));
                                }
                            }
                        }
                        foreach (var dgvr in dgvrowsNewAccessions)
                        {
                            if (dgvr.ErrorText == "")
                            {
                                string get_accession_inventory_params = ":accnumber=" + getAccessionNumber(dgvr.Cells[0].Value.ToString());
                                DataSet dstemp = _sharedUtils.GetWebServiceData("accession_batch_wizard_get_inventoryid", get_accession_inventory_params, 0, 0);
                                if (dstemp != null && dstemp.Tables["accession_batch_wizard_get_inventoryid"] != null &&
                                    dstemp.Tables["accession_batch_wizard_get_inventoryid"].Rows.Count > 0)
                                {
                                    dgvr.Cells["accession_id"].Value = dstemp.Tables["accession_batch_wizard_get_inventoryid"].Rows[0].Field<int>("accession_id");
                                    dgvr.Cells["inventory_id"].Value = dstemp.Tables["accession_batch_wizard_get_inventoryid"].Rows[0].Field<int>("inventory_id");
                                }
                                else
                                    dgvr.ErrorText = "An error ocurred while saving accession";
                            }
                        }
                    }
                    #endregion

                    DataSet dsObservationInsert = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_cto_insert", parameters, 0, 0);
                    DataTable dtObservationInsert = null;
                    if (dsObservationInsert != null && dsObservationInsert.Tables.Contains("acc_batch_wizard_get_cto_insert"))
                    {
                        dtObservationInsert = dsObservationInsert.Tables["acc_batch_wizard_get_cto_insert"];
                    }

                    /* At this moment every valid cipnumber must have an accession_id and inventory_id */
                    //Indicate Changes
                    foreach (DataGridViewRow row in datagridviewContainer.Rows)
                    {
                        if (row.Cells[0].Value == null || row.IsNewRow)
                            continue;

                        if (row.ErrorText == "")
                        {
                            for (int i = startDescriptorsIndex; i < row.Cells.Count; i++)
                            {
                                DataGridViewCell cell = row.Cells[i];
                                string inventory_id = row.Cells["inventory_id"].Value.ToString();
                                string crop_trait_id = cell.OwningColumn.Name;

                                cell.Style.BackColor = Color.White;
                                cell.ErrorText = "";
                                //Identify if column is a real crop_trait_id(Crop_trait record) or not (PASSPORT trait)
                                //Passport Trait Columns have a ColumnTag.Category = "PASSPORT"

                                switch (((ColumnTag)cell.OwningColumn.Tag).Category) {
                                    case "GROUP_TRAIT":
                                        switch (crop_trait_id)
                                        {
                                            case "13. Country of origin":
                                                #region accession_source_geography_id
                                                if (!string.IsNullOrEmpty(row.Cells["13. Country of origin"].Value.ToString()) || !string.IsNullOrEmpty(row.Cells["Adm1"].Value.ToString()) 
                                                    || !string.IsNullOrEmpty(row.Cells["Adm2"].Value.ToString()) || !string.IsNullOrEmpty(row.Cells["Adm3"].Value.ToString()))
                                                {
                                                    string country_code = row.Cells["13. Country of origin"].Value.ToString();
                                                    string adm1 = row.Cells["Adm1"].Value.ToString();
                                                    string adm2 = row.Cells["Adm2"].Value.ToString();
                                                    string adm3 = row.Cells["Adm3"].Value.ToString();
                                                    string geography_name = country_code + (string.IsNullOrEmpty(adm1) ? "" : ", " + adm1) + 
                                                        (string.IsNullOrEmpty(adm2) ? "" : ", " + adm2) + (string.IsNullOrEmpty(adm3) ? "" : ", " + adm3);


                                                    var sqlgeography_id = "select value_member as geography_id from geography_lookup where display_member = '" + geography_name + "'";
                                                    DataTable dtGeographyId = _sharedUtils.GetLocalData(sqlgeography_id, "");
                                                    
                                                    if (dtGeographyId != null && dtGeographyId.Rows.Count == 1)
                                                    {
                                                        DataRow dr = dtTableUpdate.NewRow();
                                                        dr["table_name"] = "accession_source";
                                                        dr["table_id"] = row.Cells["accession_id"].Value;
                                                        dr["filter_column"] = "source_type_code";
                                                        dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                        dr["column_name"] = "geography_id";
                                                        dr["column_type"] = "int";
                                                        dr["int_value"] = dtGeographyId.Rows[0].Field<int>("geography_id");
                                                        dr["cell"] = cell;
                                                        dtTableUpdate.Rows.Add(dr);
                                                        
                                                    }
                                                    else if (dtGeographyId.Rows.Count == 0)
                                                    {
                                                        foreach (var column_name in GroupColumnMan["accession_source_geography_id"].RequiredColumns)
                                                            row.Cells[column_name].ErrorText = "Geography record not found";
                                                    }
                                                    else if (dtGeographyId.Rows.Count > 1)
                                                        foreach (var column_name in GroupColumnMan["accession_source_geography_id"].RequiredColumns)
                                                            row.Cells[column_name].ErrorText = "More than one Geography record found";
                                                    else
                                                        foreach (var column_name in GroupColumnMan["accession_source_geography_id"].RequiredColumns)
                                                            row.Cells[column_name].ErrorText = "There was an error searching Geography id";
                                                    
                                                }
                                                #endregion
                                                break;
                                            case "":
                                                break;
                                        }

                                        break;
                                    case "PASSPORT":
                                        #region PASSPORT
                                        switch (crop_trait_id)
                                        {
                                            case "03.1.1": //03.1.1 Collecting number
                                                #region 03.1.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "COLLECTOR";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "03.1.2": //03.1.2 Breeder code
                                                #region 03.1.2
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "BREEDER";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "03.1.3": //03.1.3 Biosafety code
                                                #region 03.1.3
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "ACCESSION";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "04.2": //04.2 Collecting mission identifier
                                                #region 04.2
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "EXPLOREID";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "11.": //11. Accession name
                                                #region 11.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "CULTIVAR";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "24.": //24. Other identification associated with the accession
                                                #region 24.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "OTHER";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "23.": //23. Donor accession number
                                                #region 23.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_inv_name";
                                                    dr["table_id"] = row.Cells["inventory_id"].Value;
                                                    dr["filter_column"] = "category_code";
                                                    dr["filter_value"] = "DONOR";
                                                    dr["column_name"] = "plant_name";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                                
                                            case "15.1": //15.1 Latitude of collecting site
                                                #region 15.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "latitude";
                                                    dr["column_type"] = "decimal";
                                                    dr["decimal_value"] = decimal.Parse(cell.Value.ToString());
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "15.3": //15.3 Longitude of collecting site
                                                #region 15.3
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "longitude";
                                                    dr["column_type"] = "decimal";
                                                    dr["decimal_value"] = decimal.Parse(cell.Value.ToString());
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "16.": //16. Elevation of collecting site
                                                #region 16.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "elevation_meters";
                                                    dr["column_type"] = "int";
                                                    dr["int_value"] = int.Parse(cell.Value.ToString());
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "17.": //17. Collecting date of sample
                                                #region 17.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    string dateValue = cell.Value.ToString();
                                                    string source_date = string.Empty;
                                                    string source_date_code = string.Empty;

                                                    if (dateValue.EndsWith("0000"))
                                                    {
                                                        source_date_code = "'yyyy'";
                                                        source_date = dateValue.Substring(0, 4) + "0101";
                                                    }
                                                    else if (dateValue.EndsWith("00"))
                                                    {
                                                        source_date_code = "MM/yyyy";
                                                        source_date = dateValue.Substring(0, 6) + "01";
                                                    }
                                                    else
                                                    {
                                                        source_date_code = "MM/dd/yyyy";
                                                        source_date = dateValue;
                                                    }
                                                    //source_date  datetime  , source_date_code string
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_source";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["filter_column"] = "source_type_code";
                                                    dr["filter_value"] = row.Cells["source_type_code"].Value.ToString();
                                                    dr["column_name"] = "source_date";
                                                    dr["column_type"] = "datetime";
                                                    dr["datetime_value"] = DateTime.ParseExact(source_date, "yyyyMMdd", null);
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            //source_type_code COLLECTED

                                            case "18.1": // 18.1 Breeding institute name
                                                #region 18.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    if (dtCooperatorOrganization != null && dtCooperatorOrganization.Rows.Count > 0)
                                                    {
                                                        var rows = dtCooperatorOrganization.Select("organization  = '" + cell.Value + "'");

                                                        if (rows != null && rows.Count() == 1)
                                                        {
                                                            DataRow dr = dtTableUpdate.NewRow();
                                                            dr["table_name"] = "accession_source_map";
                                                            dr["table_id"] = row.Cells["accession_id"].Value;
                                                            dr["column_name"] = "cooperator_id";
                                                            dr["column_type"] = "int";
                                                            dr["int_value"] = rows[0].Field<int>("cooperator_id");
                                                            dr["cell"] = cell;
                                                            dr["second_table_id"] = -1;
                                                            dtTableUpdate.Rows.Add(dr);

                                                            cell.Style.BackColor = Color.Yellow;
                                                        }
                                                        else if (rows.Count() == 0)
                                                        {
                                                            cell.ErrorText = "There was an error saving cooperator";
                                                        }
                                                        else if (rows.Count() > 1)
                                                            cell.ErrorText = "More than one cooperator found";
                                                        else
                                                            cell.ErrorText = "There was an error searching cooperator id";
                                                    }
                                                    else
                                                        cell.ErrorText = "There was an error searching Institute name";
                                                }
                                                #endregion
                                                break;
                                            //source_type_code DEVELOPED


                                            case "20.1": //20.1 Parent Female
                                                #region 20.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_pedigree";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "female_external_accession";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "20.2": //20.2 Parent Male
                                                #region 20.2
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_pedigree";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "male_external_accession";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "20.3": //20.3 Parent Female (Accenumb)
                                                #region 20.3
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    var accessionLookupRows = dtAccessionInventoryLookup.Select("AccNumber = '" + cell.Value.ToString().Replace("CIP ","") + "'");

                                                    if (accessionLookupRows != null && accessionLookupRows.Count() == 1)
                                                    {
                                                        DataRow dr = dtTableUpdate.NewRow();
                                                        dr["table_name"] = "accession_pedigree";
                                                        dr["table_id"] = row.Cells["accession_id"].Value;
                                                        dr["column_name"] = "female_accession_id";
                                                        dr["column_type"] = "int";
                                                        dr["int_value"] = accessionLookupRows[0].Field<int>("accession_id");
                                                        dr["cell"] = cell;
                                                        dtTableUpdate.Rows.Add(dr);
                                                    }
                                                    else if (accessionLookupRows.Count() == 0)
                                                    {
                                                        cell.ErrorText = "Accession not found";
                                                    }
                                                    else if (accessionLookupRows.Count() > 1)
                                                        cell.ErrorText = "More than one Accession found";
                                                    else
                                                        cell.ErrorText = "There was an error searching accession id";
                                                }
                                                #endregion
                                                break;
                                            case "20.4": //20.4 Parent Male (Accenumb)
                                                #region 20.4
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    var accessionLookupRows = dtAccessionInventoryLookup.Select("AccNumber = '" + cell.Value.ToString().Replace("CIP ", "") + "'");

                                                    if (accessionLookupRows != null && accessionLookupRows.Count() == 1)
                                                    {
                                                        DataRow dr = dtTableUpdate.NewRow();
                                                        dr["table_name"] = "accession_pedigree";
                                                        dr["table_id"] = row.Cells["accession_id"].Value;
                                                        dr["column_name"] = "male_accession_id";
                                                        dr["column_type"] = "int";
                                                        dr["int_value"] = accessionLookupRows[0].Field<int>("accession_id");
                                                        dr["cell"] = cell;
                                                        dtTableUpdate.Rows.Add(dr);
                                                    }
                                                    else if (accessionLookupRows.Count() == 0)
                                                    {
                                                        cell.ErrorText = "Accession not found";
                                                    }
                                                    else if (accessionLookupRows.Count() > 1)
                                                        cell.ErrorText = "More than one Accession found";
                                                    else
                                                        cell.ErrorText = "There was an error searching accession id";
                                                }
                                                #endregion
                                                break;


                                            case "12.": //12. Acquisition date
                                                #region 12.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    string dateValue = cell.Value.ToString();
                                                    string source_date = string.Empty;
                                                    string source_date_code = string.Empty;

                                                    if (dateValue.EndsWith("0000"))
                                                    {
                                                        source_date_code = "'yyyy'";
                                                        source_date = dateValue.Substring(0, 4) + "0101";
                                                    }
                                                    else if (dateValue.EndsWith("00"))
                                                    {
                                                        source_date_code = "MM/yyyy";
                                                        source_date = dateValue.Substring(0, 6) + "01";
                                                    }
                                                    else
                                                    {
                                                        source_date_code = "MM/dd/yyyy";
                                                        source_date = dateValue;
                                                    }
                                                    //initial_received_date  datetime  , initial_received_date_code string
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "initial_received_date";
                                                    dr["column_type"] = "datetime";
                                                    dr["datetime_value"] = DateTime.ParseExact(source_date, "yyyyMMdd", null);
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "19.": //19. Biological Status
                                                #region 19.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "improvement_status_code";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "28.": //28. Remarks
                                                #region 28.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "note";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "00.1": //Genebank Accession Status
                                                #region 00.1
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    dr["column_name"] = "status_code";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;

                                            case "27.": //27. MLS status of the accession
                                                #region 27.
                                                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                                                {
                                                    DataRow dr = dtTableUpdate.NewRow();
                                                    dr["table_name"] = "accession_ipr";
                                                    dr["table_id"] = row.Cells["accession_id"].Value;
                                                    /*dr["filter_column"] = "type_code";
                                                    dr["filter_value"] = "MLS";*/
                                                    dr["column_name"] = "ipr_number";
                                                    dr["column_type"] = "string";
                                                    dr["string_value"] = cell.Value.ToString();
                                                    dr["cell"] = cell;
                                                    dtTableUpdate.Rows.Add(dr);
                                                }
                                                #endregion
                                                break;
                                            case "": //
                                                break;
                                        }
                                        #endregion
                                        break;
                                    case "CROP_TRAIT":
                                        #region crop_trait_observation
                                        var observation = dtObservationUpdate.Select("inventory_id = " + inventory_id + " and crop_trait_id = " + crop_trait_id);

                                        if (observation != null && observation.Count() == 1 && !string.IsNullOrEmpty(cell.Value.ToString()))
                                        {
                                            var crop_trait_type = dtCropTraitIsCoded.Select("crop_trait_id = " + crop_trait_id);

                                            observation[0].SetField<string>("is_archived", ux_checkboxIsArchived.Checked ? "Y" : "N");

                                            string type_column = string.Empty;
                                            switch (crop_trait_type[0].Field<string>("type"))
                                            {
                                                case "CODED":
                                                    DataTable dtCropTraitCode = _sharedUtils.GetLocalData("select value_member as crop_trait_code_id from crop_trait_code_lookup where crop_trait_id = " +
                                                        crop_trait_id + " and code = '" + cell.Value.ToString() + "'", "");
                                                    int new_crop_trait_code_id = -1;
                                                    if (dtCropTraitCode != null && dtCropTraitCode.Rows.Count == 1)
                                                    {
                                                        new_crop_trait_code_id = dtCropTraitCode.Rows[0].Field<int>("crop_trait_code_id");

                                                        int code_trait_code_id = -1;
                                                        try
                                                        {
                                                            code_trait_code_id = observation[0].Field<int>("crop_trait_code_id");
                                                        } catch
                                                        { code_trait_code_id = -1; }

                                                        if (code_trait_code_id == -1 || code_trait_code_id != new_crop_trait_code_id)
                                                        {
                                                            cell.Style.BackColor = Color.Orange;
                                                            cell.Tag = "MODIFIED";
                                                            observation[0].SetField<int>("crop_trait_code_id", new_crop_trait_code_id);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cell.ErrorText = "Value is not accepted";
                                                        continue;
                                                    }
                                                    type_column = "crop_trait_code_id";

                                                    break;
                                                case "CHAR":
                                                    type_column = "string_value";
                                                    var string_value = observation[0].Field<string>("string_value");
                                                    var new_string_value = cell.Value.ToString();
                                                    if (!new_string_value.Trim().Equals(string_value.Trim()))
                                                    {
                                                        cell.Style.BackColor = Color.Orange;
                                                        cell.Tag = "MODIFIED";
                                                        observation[0].SetField<string>("string_value", new_string_value);
                                                    }
                                                    break;
                                                case "NUMERIC":
                                                    type_column = "numeric_value";
                                                    try
                                                    {
                                                        var numeric_value = observation[0].Field<decimal>("numeric_value");
                                                        var new_numeric_value = decimal.Parse(cell.Value.ToString());
                                                        if (new_numeric_value != numeric_value)
                                                        {
                                                            cell.Style.BackColor = Color.Orange;
                                                            cell.Tag = "MODIFIED";
                                                            observation[0].SetField<decimal>("numeric_value", new_numeric_value);
                                                        }
                                                    }
                                                    catch (Exception numeric_error)
                                                    {
                                                        cell.ErrorText = numeric_error.Message;
                                                        continue;
                                                    }
                                                    break;
                                            }
                                        }
                                        else if ((observation == null || observation.Count() == 0) && !string.IsNullOrEmpty(cell.Value.ToString()))
                                        {
                                            // insert observation
                                            DataRow dr = dtObservationInsert.NewRow();

                                            dr["inventory_id"] = inventory_id;
                                            dr["crop_trait_id"] = crop_trait_id;
                                            dr["crop_trait_code_id"] = DBNull.Value;
                                            dr["numeric_value"] = DBNull.Value;
                                            dr["string_value"] = DBNull.Value;
                                            dr["method_id"] = _method_id;
                                            dr["is_archived"] = ux_checkboxIsArchived.Checked ? "Y" : "N";
                                            string sNow = DateTime.Now.ToString();
                                            dr["created_by"] = _sharedUtils.UserCooperatorID;
                                            dr["created_date"] = sNow;
                                            dr["owned_by"] = _sharedUtils.UserCooperatorID;
                                            dr["owned_date"] = sNow;

                                            var crop_trait_type = dtCropTraitIsCoded.Select("crop_trait_id = " + crop_trait_id);

                                            string type_column = string.Empty;
                                            switch (crop_trait_type[0].Field<string>("type"))
                                            {
                                                case "CODED":
                                                    DataTable dtCropTraitCode = _sharedUtils.GetLocalData("select value_member as crop_trait_code_id from crop_trait_code_lookup where crop_trait_id = " +
                                                        crop_trait_id + " and code = '" + cell.Value.ToString() + "'", "");

                                                    if (dtCropTraitCode != null && dtCropTraitCode.Rows.Count == 1)
                                                        dr["crop_trait_code_id"] = dtCropTraitCode.Rows[0].Field<int>("crop_trait_code_id");
                                                    else
                                                    {
                                                        cell.ErrorText = "Value is not accepted";
                                                        continue;
                                                    }
                                                    break;
                                                case "CHAR":
                                                    dr["string_value"] = cell.Value.ToString();
                                                    break;
                                                case "NUMERIC":
                                                    dr["numeric_value"] = decimal.Parse(cell.Value.ToString());
                                                    break;
                                            }

                                            dtObservationInsert.Rows.Add(dr);

                                            cell.Style.BackColor = Color.Orange;
                                            cell.Tag = "MODIFIED";
                                        }
                                        #endregion
                                        break;
                                }
                                
                                /*
                                if (crop_trait_id.IndexOf('.') > -1)
                                { // fake crop_trait_id has at least one '.'
                                    
                                }
                                else
                                { // real crop_trait_id is a integer
                                    
                                }*/

                            }
                        }//end_if
                    }//end_foreach

                    //Check if there are accessions updates
                    #region save_table_accession
                    var accessionUpdateRows = dtTableUpdate.Select("table_name = 'accession'");
                    if (accessionUpdateRows.Count() > 0)
                    {
                        string dataview_name = "acc_batch_wizard_get_accession_update";
                        string[] columnNames = new string[] { "taxonomy_species_name", "12.", "19.", "28.", "00.1" };
                        string search_index_column = "accession_id";

                        List<int> accessionListUpdate = new List<int>();
                        foreach (DataRow r in accessionUpdateRows) accessionListUpdate.Add(r.Field<int>("table_id"));

                        DataSet dsAccessionUpdate = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionListUpdate.ConvertAll(x => x.ToString()).ToArray()), 0, 0);
                        if (dsAccessionUpdate != null && dsAccessionUpdate.Tables[dataview_name] != null)
                        {
                            DataTable dtAccessionUpdate = dsAccessionUpdate.Tables[dataview_name];
                            foreach (DataRow r in accessionUpdateRows)
                            {
                                DataRow rupdate = dtAccessionUpdate.Select("accession_id = " + r.Field<int>("table_id"))[0];
                                switch (r.Field<string>("column_type"))
                                {
                                    case "int":
                                        if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                            || rupdate.Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                        {
                                            rupdate.SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                        break;
                                    case "string":
                                        if (string.IsNullOrEmpty(rupdate.Field<string>(r.Field<string>("column_name")))
                                            || !rupdate.Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                        {
                                            rupdate.SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                        break;
                                    case "decimal":
                                        if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                            || rupdate.Field<decimal>(r.Field<string>("column_name")) != r.Field<decimal>("decimal_value"))
                                        {
                                            rupdate.SetField<decimal>(r.Field<string>("column_name"), r.Field<decimal>("decimal_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                        break;
                                    case "datetime":
                                        if (rupdate.ItemArray[dtAccessionUpdate.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                            || rupdate.Field<DateTime>(r.Field<string>("column_name")) != r.Field<DateTime>("datetime_value"))
                                        {
                                            rupdate.SetField<DateTime>(r.Field<string>("column_name"), r.Field<DateTime>("datetime_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                        break;
                                }
                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionUpdate);
                            if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                            {
                                foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                {
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();

                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName))
                                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //send updates
                    #region save_crop_trait_observation_updates
                    if (true) // check if updates exists
                    {
                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsObservationUpdate);
                        if (saveErrors != null && saveErrors.Tables.Contains(get_crop_trait_observation_update))
                        {
                            foreach (DataRow dr in saveErrors.Tables[get_crop_trait_observation_update].Rows)
                            {
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                        , /*dr["cipnumber"].ToString()*/"", dr["ExceptionMessage"].ToString()));
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                    row.Cells[dr["crop_trait_id"].ToString()].ErrorText = dr["ExceptionMessage"].ToString();

                                    /*MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));*/
                                }
                                else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                    row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = Color.LightBlue;

                                }
                            }
                        }
                    }
                    #endregion
                    //send inserts
                    #region save_crop_trait_observation_inserts
                    if (dtObservationInsert.Rows.Count > 0) // check if updates exists
                    {
                        DataSet saveErrors = new DataSet();
                        saveErrors = _sharedUtils.SaveWebServiceData(dsObservationInsert);
                        if (saveErrors != null && saveErrors.Tables.Contains("acc_batch_wizard_get_cto_insert"))
                        {
                            foreach (DataRow dr in saveErrors.Tables["acc_batch_wizard_get_cto_insert"].Rows)
                            {
                                if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                    row.Cells[dr["crop_trait_id"].ToString()].ErrorText = dr["ExceptionMessage"].ToString();

                                    /*MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));*/
                                }
                                else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    /*MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));*/
                                }
                                else if (dr["SavedAction"].ToString() == "Delete" && dr["SavedStatus"].ToString() != "Success")
                                {
                                    /*MessageBox.Show(string.Format("The {0} item could not be successfully deleted from your list.\n\nError message:\n\n{1}"
                                        , "", dr["ExceptionMessage"].ToString()));*/
                                }
                                else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                    //row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = Color.Green;
                                    row.Cells[dr["crop_trait_id"].ToString()].Style.BackColor = Color.LightBlue;
                                }
                            }
                        }
                    }
                    #endregion

                    //save accession_inv_name "11. Accession name" updates and inserts
                    #region save_accession_inv_name_CULTIVAR
                    var accessionInvNameCultivarRows = dtTableUpdate.Select("table_name = 'accession_inv_name' and filter_value = 'CULTIVAR'");
                    if (accessionInvNameCultivarRows.Count() > 0)
                    {
                        List<int> inventoryList = new List<int>();
                        foreach (DataRow r in accessionInvNameCultivarRows)
                        {
                            inventoryList.Add(r.Field<int>("table_id"));
                        }
                        DataSet dsAccessionInvName = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_acc_inv_name_update", ":inventoryid=" + string.Join(",", inventoryList.ConvertAll(x => x.ToString()).ToArray()) + ";:categorycode=CULTIVAR", 0, 0);
                        if (dsAccessionInvName != null && dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"] != null)
                        {
                            DataTable dtAccessionInvName = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"];
                            foreach (DataRow r in accessionInvNameCultivarRows)
                            {
                                DataRow[] rupdate = dtAccessionInvName.Select("inventory_id = " + r.Field<int>("table_id"));
                                if (rupdate.Count() == 0)
                                {
                                    DataRow dr = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].NewRow();
                                    dr["inventory_id"] = r.Field<int>("table_id");
                                    dr["category_code"] = "CULTIVAR";
                                    dr["plant_name"] = r.Field<string>("string_value");
                                    dr["plant_name_rank"] = 0;
                                    dr["is_web_visible"] = "Y";
                                    dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows.Add(dr);

                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                }
                                else
                                {
                                    if (!rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                    {
                                        rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                }

                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionInvName);
                            if (saveErrors != null && saveErrors.Tables.Contains("acc_batch_wizard_get_acc_inv_name_update"))
                            {
                                foreach (DataRow dr in saveErrors.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows)
                                {
                                    if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                        row.Cells["11."].ErrorText = dr["ExceptionMessage"].ToString();
                                        MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                            , dr["cipnumber"].ToString(), dr["ExceptionMessage"].ToString()));
                                    }
                                    else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                        //row.Cells["11."].Style.BackColor = Color.Green;
                                        row.Cells["11."].Style.BackColor = Color.LightBlue;
                                        row.Cells["11."].ErrorText = string.Empty;
                                    }
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                        row.Cells["11."].ErrorText = dr["ExceptionMessage"].ToString();
                                        MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                            , "", dr["ExceptionMessage"].ToString()));
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                                        row.Cells["11."].Style.BackColor = Color.LightBlue;
                                        row.Cells["11."].ErrorText = string.Empty;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //save accession_inv_name "03.1.1 Collecting number" updates and inserts
                    #region save_accession_inv_name_COLLECTOR
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "COLLECTOR", 3);
                    #endregion

                    //save accession_inv_name "03.1.2 Breeder code" updates and inserts
                    #region save_accession_inv_name_BREEDER
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "BREEDER", 3);
                    #endregion

                    //save accession_inv_name "03.1.3 Biosafety code" updates and inserts
                    #region save_accession_inv_name_ACCESSION
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "ACCESSION", 3);
                    #endregion

                    //save accession_inv_name "04.2 Collecting mission identifier" updates and inserts
                    #region save_accession_inv_name_EXPLOREID
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "EXPLOREID", 5);
                    #endregion

                    //save accession_inv_name "24. Other identification associated with the accession" updates and inserts
                    #region save_accession_inv_name_DONOR
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "DONOR", 1);
                    #endregion

                    //save accession_inv_name "24. Other identification associated with the accession" updates and inserts
                    #region save_accession_inv_name_OTHER
                    SaveAccessionInvName(datagridviewContainer, dtTableUpdate, "OTHER", 2);
                    #endregion

                    //save accession_inv_name "27. MLS status of the accession" updates and inserts
                    #region save_accession_ipr
                    if (true)
                    {
                        var accessionIPRRows = dtTableUpdate.Select("table_name = 'accession_IPR'");
                        string dataview_name = "acc_batch_wizard_get_accession_ipr";
                        if (accessionIPRRows.Count() > 0)
                        {
                            string columnName = accessionIPRRows[0].Field<DataGridViewCell>("cell").OwningColumn.Name;
                            string search_index_column = "accession_id";

                            List<int> accessionList = new List<int>();
                            foreach (DataRow r in accessionIPRRows)
                                accessionList.Add(r.Field<int>("table_id"));

                            DataSet dsAccessionIPR = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList.ConvertAll(x => x.ToString()).ToArray()), 0, 0);
                            if (dsAccessionIPR != null && dsAccessionIPR.Tables[dataview_name] != null)
                            {
                                DataTable dtAccessionIPR = dsAccessionIPR.Tables[dataview_name];
                                foreach (DataRow r in accessionIPRRows)
                                {
                                    DataRow[] rupdate = dtAccessionIPR.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                    if (rupdate.Count() == 0)
                                    {
                                        DataRow dr = dsAccessionIPR.Tables[dataview_name].NewRow();
                                        dr[search_index_column] = r.Field<int>("table_id"); //accession_id = 
                                        dr["type_code"] = "MLS"; // usar filter_col & filter_val
                                        dr[r.Field<string>("column_name")] = r.Field<string>("string_value"); // ipr_number =
                                        dsAccessionIPR.Tables[dataview_name].Rows.Add(dr);

                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    else
                                    {
                                        if (!rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                        {
                                            rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                        }
                                    }

                                }
                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionIPR);
                            if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                            {
                                foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                    if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}", "", dr["ExceptionMessage"].ToString()));
                                    }
                                    else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        //row.Cells[columnName].Style.BackColor = Color.Green;
                                        row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                        row.Cells[columnName].ErrorText = string.Empty;
                                    }
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}", "", dr["ExceptionMessage"].ToString()));
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                        row.Cells[columnName].ErrorText = string.Empty;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //save accession_pedigree
                    #region save_accession_pedigree
                    if (true)
                    {
                        var accessionPedigreeRows = dtTableUpdate.Select("table_name = 'accession_pedigree'");
                        string dataview_name = "acc_batch_wizard_get_accession_pedigree_update";
                        if (accessionPedigreeRows.Count() > 0)
                        {
                            string[] columnNames = new string[] { "20.1", "20.2", "20.3", "20.4" };
                            string search_index_column = "accession_id";

                            List<int> accessionList = new List<int>();
                            foreach (DataRow r in accessionPedigreeRows) accessionList.Add(r.Field<int>("table_id"));

                            DataSet dsAccessionPedigree = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList.ConvertAll(x => x.ToString()).ToArray()), 0, 0);
                            if (dsAccessionPedigree != null && dsAccessionPedigree.Tables[dataview_name] != null)
                            {
                                DataTable dtAccessionPedigree = dsAccessionPedigree.Tables[dataview_name];
                                foreach (DataRow r in accessionPedigreeRows)
                                {
                                    DataRow[] rupdate = dtAccessionPedigree.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                    if (rupdate.Count() == 0) //insert
                                    {
                                        DataRow dr = dsAccessionPedigree.Tables[dataview_name].NewRow();
                                        dr[search_index_column] = r.Field<int>("table_id"); //accession_id = 
                                        switch (r.Field<string>("column_type"))
                                        {
                                            case "int":
                                                dr[r.Field<string>("column_name")] = r.Field<int>("int_value"); // male_accession_id, female_accession_id
                                                break;
                                            case "string":
                                                dr[r.Field<string>("column_name")] = r.Field<string>("string_value"); // male_external_accession, female_external_accession
                                                break;
                                        }
                                        dsAccessionPedigree.Tables[dataview_name].Rows.Add(dr);

                                        (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                    }
                                    else //update
                                    {
                                        switch (r.Field<string>("column_type"))
                                        {
                                            case "int":
                                                if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                    || rupdate[0].Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                                {
                                                    rupdate[0].SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value")); // male_accession_id, female_accession_id
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                            case "string":
                                                if (string.IsNullOrEmpty(rupdate[0].Field<string>(r.Field<string>("column_name")))
                                                    || !rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                                {
                                                    rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value")); // male_external_accession, female_external_accession
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                        }
                                    }
                                }
                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionPedigree);
                            if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                            {
                                foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                    if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName)) row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        /*MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}", "", dr["ExceptionMessage"].ToString()));*/
                                    }
                                    else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                //row.Cells[columnName].Style.BackColor = Color.Green;
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName))
                                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        /*MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}", "", dr["ExceptionMessage"].ToString()));*/
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //save accession_source
                    #region save_accession_source
                    if (true)
                    {
                        var accessionPedigreeRows = dtTableUpdate.Select("table_name = 'accession_source'");
                        string dataview_name = "acc_batch_wizard_get_accession_source_update";
                        if (accessionPedigreeRows.Count() > 0)
                        {
                            string[] columnNames = new string[] { "13.", "15.1", "15.3", "16.", "17.", "13. Country of origin", "Adm1", "Adm2", "Adm3" };
                            string search_index_column = "accession_id";

                            List<int> accessionList = new List<int>();
                            foreach (DataRow r in accessionPedigreeRows) accessionList.Add(r.Field<int>("table_id"));

                            DataSet dsAccessionPedigree = _sharedUtils.GetWebServiceData(dataview_name, ":accessionid=" + string.Join(",", accessionList.ConvertAll(x => x.ToString()).ToArray()), 0, 0);
                            if (dsAccessionPedigree != null && dsAccessionPedigree.Tables[dataview_name] != null)
                            {
                                DataTable dtAccessionPedigree = dsAccessionPedigree.Tables[dataview_name];
                                foreach (DataRow r in accessionPedigreeRows)
                                {
                                    DataRow[] rupdate = dtAccessionPedigree.Select(search_index_column + " = " + r.Field<int>("table_id"));
                                    if (rupdate.Count() == 0) //insert
                                    {
                                        DataGridViewRow dgvRow = r.Field<DataGridViewCell>("cell").OwningRow;
                                        if (dgvRow.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(dgvRow.Cells["source_type_code"].Value.ToString()))
                                        {
                                            DataRow dr = dsAccessionPedigree.Tables[dataview_name].NewRow();
                                            dr[search_index_column] = r.Field<int>("table_id"); //accession_id
                                            dr["source_type_code"] = dgvRow.Cells["source_type_code"].Value;
                                            dr["is_origin"] = "Y";
                                            dr["is_web_visible"] = "Y";

                                            switch (r.Field<string>("column_type"))
                                            {
                                                case "int":
                                                    dr[r.Field<string>("column_name")] = r.Field<int>("int_value");
                                                    break;
                                                case "string":
                                                    dr[r.Field<string>("column_name")] = r.Field<string>("string_value");
                                                    break;
                                                case "decimal":
                                                    dr[r.Field<string>("column_name")] = r.Field<decimal>("decimal_value");
                                                    break;
                                                case "datetime":
                                                    dr[r.Field<string>("column_name")] = r.Field<DateTime>("datetime_value");
                                                    break;
                                            }
                                            dsAccessionPedigree.Tables[dataview_name].Rows.Add(dr);
                                            
                                            if (r.Field<string>("column_name").Equals("geography_id")) //GroupColumnMan.ContainsKey(table_name + column_name)
                                            {
                                                DataGridViewRow row = r.Field<DataGridViewCell>("cell").OwningRow;
                                                foreach (var column_name in GroupColumnMan["accession_source_geography_id"].RequiredColumns)
                                                    row.Cells[column_name].Style.BackColor = Color.Orange;
                                            }
                                            else
                                                r.Field<DataGridViewCell>("cell").Style.BackColor = Color.Orange;
                                        }
                                        else
                                            r.Field<DataGridViewCell>("cell").ErrorText = "source_type_code must be COLLECTED, DEVELOPED or DONATED";
                                    }
                                    else //update
                                    {
                                        switch (r.Field<string>("column_type"))
                                        {
                                            case "int":
                                                if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                    || rupdate[0].Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                                {
                                                    rupdate[0].SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value"));

                                                    if (r.Field<string>("column_name").Equals("geography_id")) //GroupColumnMan.ContainsKey(table_name + column_name)
                                                    {
                                                        DataGridViewRow row = r.Field<DataGridViewCell>("cell").OwningRow;
                                                        foreach (var column_name in GroupColumnMan["accession_source_geography_id"].RequiredColumns)
                                                            row.Cells[column_name].Style.BackColor = Color.Orange;
                                                    }
                                                    else
                                                        r.Field<DataGridViewCell>("cell").Style.BackColor = Color.Orange;
                                                }
                                                break;
                                            case "string":
                                                if (string.IsNullOrEmpty(rupdate[0].Field<string>(r.Field<string>("column_name")))
                                                    || !rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                                                {
                                                    rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                            case "decimal":
                                                if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                    || rupdate[0].Field<decimal>(r.Field<string>("column_name")) != r.Field<decimal>("decimal_value"))
                                                {
                                                    rupdate[0].SetField<decimal>(r.Field<string>("column_name"), r.Field<decimal>("decimal_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                            case "datetime":
                                                if (rupdate[0].ItemArray[dtAccessionPedigree.Columns[r.Field<string>("column_name")].Ordinal] == DBNull.Value
                                                    || rupdate[0].Field<DateTime>(r.Field<string>("column_name")) != r.Field<DateTime>("datetime_value"))
                                                {
                                                    rupdate[0].SetField<DateTime>(r.Field<string>("column_name"), r.Field<DateTime>("datetime_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                break;
                                        }
                                    }
                                }
                            }

                            DataSet saveErrors = new DataSet();
                            saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionPedigree);
                            if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                            {
                                foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                {
                                    DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells[search_index_column].Value.ToString().Equals(dr[search_index_column].ToString())).First();
                                    if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName))
                                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                    }
                                    else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                //row.Cells[columnName].Style.BackColor = Color.Green;
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                    if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName))
                                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                    }
                                    else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                    {
                                        foreach (var columnName in columnNames)
                                            if (datagridviewContainer.Columns.Contains(columnName) && row.Cells[columnName].Style.BackColor == Color.Orange)
                                            {
                                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                                row.Cells[columnName].ErrorText = string.Empty;
                                            }
                                    }
                                }
                            }
                        }
                    }
                    #endregion


                    //save accession_source_map
                    #region save_accession_source_map
                    if (true)
                    {
                        var accessionAccessionSourceMapRows = dtTableUpdate.Select("table_name = 'accession_source_map'");
                        //validar accession_source_ids

                        string dataview_name_acc_source = "acc_batch_wizard_get_accession_source_update";
                        if (accessionAccessionSourceMapRows.Count() > 0)
                        {
                            string search_index_column_acc_source = "accession_id";

                            List<int> accessionList = new List<int>();
                            foreach (DataRow r in accessionAccessionSourceMapRows) accessionList.Add(r.Field<int>("table_id"));

                            DataSet dsAccessionSource = _sharedUtils.GetWebServiceData(dataview_name_acc_source, ":accessionid=" + string.Join(",", accessionList.ConvertAll(x => x.ToString()).ToArray()), 0, 0);
                            if (dsAccessionSource != null && dsAccessionSource.Tables[dataview_name_acc_source] != null)
                            {
                                DataTable dtAccessionSource = dsAccessionSource.Tables[dataview_name_acc_source];
                                foreach (DataRow r in accessionAccessionSourceMapRows)
                                {
                                    DataRow[] rupdate = dtAccessionSource.Select(search_index_column_acc_source + " = " + r.Field<int>("table_id")); //search accesion_source by accession_id
                                    if (rupdate.Count() == 0) //insert
                                    {
                                        DataGridViewRow dgvRow = r.Field<DataGridViewCell>("cell").OwningRow;
                                        if (dgvRow.Cells["source_type_code"].Value != null && !string.IsNullOrEmpty(dgvRow.Cells["source_type_code"].Value.ToString()))
                                        {
                                            DataRow dr = dsAccessionSource.Tables[dataview_name_acc_source].NewRow();
                                            dr[search_index_column_acc_source] = r.Field<int>("table_id"); //accession_id
                                            dr["source_type_code"] = dgvRow.Cells["source_type_code"].Value;
                                            dr["is_origin"] = "Y";
                                            dr["is_web_visible"] = "Y";
                                            dsAccessionSource.Tables[dataview_name_acc_source].Rows.Add(dr);
                                        }
                                        else
                                        {
                                            r.Field<DataGridViewCell>("cell").ErrorText = "source_type_code must be COLLECTED, DEVELOPED or DONATED";
                                            r.SetField<int>("table_id", -1); //not valid accession_id
                                        }
                                    }
                                    else // exists
                                    {
                                        r.Field<DataGridViewCell>("cell").Style.BackColor = Color.LightGoldenrodYellow;
                                    }
                                }
                            }

                            _sharedUtils.SaveWebServiceData(dsAccessionSource);
                            dsAccessionSource = _sharedUtils.GetWebServiceData(dataview_name_acc_source, ":accessionid=" + string.Join(",", accessionList.ConvertAll(x => x.ToString()).ToArray()), 0, 0);
                            /* Change accession_ids by acession_source_ids */
                            if (dsAccessionSource != null && dsAccessionSource.Tables[dataview_name_acc_source] != null)
                            {
                                DataTable dtAccessionSource = dsAccessionSource.Tables[dataview_name_acc_source];
                                foreach (DataRow r in accessionAccessionSourceMapRows)
                                {
                                    if (string.IsNullOrEmpty(r.Field<DataGridViewCell>("cell").ErrorText))
                                    {
                                        var rAccSource = dtAccessionSource.Select(search_index_column_acc_source + " = " + r.Field<int>("table_id")); //search accesion_source by accession_id
                                        if (rAccSource.Count() == 1)
                                        {
                                            r.SetField<int>("second_table_id", rAccSource[0].Field<int>("accession_source_id")); //now table_id = accession_source_id
                                            r.Field<DataGridViewCell>("cell").Style.BackColor = Color.LightGoldenrodYellow;
                                        }
                                        else
                                        {
                                            r.Field<DataGridViewCell>("cell").ErrorText = "There was an error saving Accession Source";
                                        }
                                    }
                                }
                                #region save_accession_source_map_reg

                                /****** accession_source_map ********/
                                string dataview_name = "acc_batch_wizard_get_accession_source_map";
                                string columnName = accessionAccessionSourceMapRows[0].Field<DataGridViewCell>("cell").OwningColumn.Name;
                                string search_index_column = "accession_source_id";

                                List<int> accessionSourceList = new List<int>();
                                foreach (DataRow r in accessionAccessionSourceMapRows)
                                {
                                    if (string.IsNullOrEmpty(r.Field<DataGridViewCell>("cell").ErrorText))
                                        accessionSourceList.Add(r.Field<int>("second_table_id"));
                                }

                                DataSet dsAccessionSourceMap = _sharedUtils.GetWebServiceData(dataview_name, ":accessionsourceid=" + string.Join(",", accessionSourceList.ConvertAll(x => x.ToString()).ToArray()), 0, 0);
                                if (dsAccessionSourceMap != null && dsAccessionSourceMap.Tables[dataview_name] != null)
                                {
                                    DataTable dtAccessionSourceMap = dsAccessionSourceMap.Tables[dataview_name];
                                    foreach (DataRow r in accessionAccessionSourceMapRows)
                                    {
                                        if (string.IsNullOrEmpty(r.Field<DataGridViewCell>("cell").ErrorText))
                                        {
                                            DataRow[] rupdate = dtAccessionSourceMap.Select(search_index_column + " = " + r.Field<int>("second_table_id"));
                                            if (rupdate.Count() == 0)
                                            {
                                                DataRow dr = dsAccessionSourceMap.Tables[dataview_name].NewRow();
                                                dr[search_index_column] = r.Field<int>("second_table_id"); // accession_source_id
                                                dr[r.Field<string>("column_name")] = r.Field<int>("int_value"); // cooperator_id
                                                dsAccessionSourceMap.Tables[dataview_name].Rows.Add(dr);

                                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                            }
                                            else
                                            {
                                                if (rupdate[0].Field<int>(r.Field<string>("column_name")) != r.Field<int>("int_value"))
                                                {
                                                    rupdate[0].SetField<int>(r.Field<string>("column_name"), r.Field<int>("int_value"));
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                                                }
                                                else
                                                {
                                                    (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.White;
                                                }
                                            }
                                        }
                                    }
                                }

                                DataSet saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionSourceMap);
                                if (saveErrors != null && saveErrors.Tables.Contains(dataview_name))
                                {
                                    foreach (DataRow dr in saveErrors.Tables[dataview_name].Rows)
                                    {
                                        var asm = accessionAccessionSourceMapRows.Where(r => r.Field<int>("second_table_id") == int.Parse(dr["accession_source_id"].ToString())).First();
                                        DataGridViewRow row = asm.Field<DataGridViewCell>("cell").OwningRow;
                                        if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                                        {
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        }
                                        else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                                        {
                                            //row.Cells[columnName].Style.BackColor = Color.Green;
                                            row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                        if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                                        {
                                            row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                        }
                                        else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                                        {
                                            row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                            row.Cells[columnName].ErrorText = string.Empty;
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion

                }//end_if(CIPNumbers.Count > 0)

                watch.Stop();

                _sharedUtils.LookupTablesUpdateTable("accession_lookup", false);
                _sharedUtils.LookupTablesUpdateTable("inventory_lookup", false);

                this.Invoke((MethodInvoker)delegate
                {
                    ux_progressBarWorking.Visible = false;
                    ux_buttonSaveData.Enabled = true;
                    ux_labelMessage.Text = "Elapsed time : " + watch.ElapsedMilliseconds + " ms";
                });
            }
            catch (Exception e) {
                this.Invoke((MethodInvoker)delegate
                {
                    ux_progressBarWorking.Visible = false;
                    ux_buttonSaveData.Enabled = true;
                    ux_labelMessage.Text = string.Empty;
                });
                MessageBox.Show(e.Message);
            }

            
        }

        private void SaveAccessionInvName(DataGridView datagridviewContainer, DataTable dtTableUpdate, string category_code, int plant_name_rank) {
            var accessionInvNameCultivarRows = dtTableUpdate.Select("table_name = 'accession_inv_name' and filter_value = '" + category_code + "'");
            if (accessionInvNameCultivarRows.Count() > 0)
            {
                List<int> inventoryList = new List<int>();
                foreach (DataRow r in accessionInvNameCultivarRows)
                {
                    inventoryList.Add(r.Field<int>("table_id"));
                }
                string[] stringInventoryList = inventoryList.ConvertAll(x => x.ToString()).ToArray();
                string columnName = accessionInvNameCultivarRows[0].Field<DataGridViewCell>("cell").OwningColumn.Name;
                //int columnIndex = accessionInvNameCultivarRows[0].Field<DataGridViewCell>("cell").ColumnIndex;

                DataSet dsAccessionInvName = _sharedUtils.GetWebServiceData("acc_batch_wizard_get_acc_inv_name_update", ":inventoryid=" + string.Join(",", stringInventoryList) + ";:categorycode=" + category_code, 0, 0);
                if (dsAccessionInvName != null && dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"] != null)
                {
                    DataTable dtAccessionInvName = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"];
                    foreach (DataRow r in accessionInvNameCultivarRows)
                    {
                        DataRow[] rupdate = dtAccessionInvName.Select("inventory_id = " + r.Field<int>("table_id"));
                        if (rupdate.Count() == 0)
                        {
                            DataRow dr = dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].NewRow();
                            dr["inventory_id"] = r.Field<int>("table_id");
                            dr["category_code"] = category_code;
                            dr["plant_name"] = r.Field<string>("string_value");
                            dr["plant_name_rank"] = plant_name_rank;
                            dr["is_web_visible"] = "Y";
                            dsAccessionInvName.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows.Add(dr);

                            (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                        }
                        else
                        {
                            if (!rupdate[0].Field<string>(r.Field<string>("column_name")).Equals(r.Field<string>("string_value")))
                            {
                                rupdate[0].SetField<string>(r.Field<string>("column_name"), r.Field<string>("string_value"));
                                (r.Field<DataGridViewCell>("cell")).Style.BackColor = Color.Orange;
                            }
                        }

                    }

                    DataSet saveErrors = new DataSet();
                    saveErrors = _sharedUtils.SaveWebServiceData(dsAccessionInvName);
                    if (saveErrors != null && saveErrors.Tables.Contains("acc_batch_wizard_get_acc_inv_name_update"))
                    {
                        foreach (DataRow dr in saveErrors.Tables["acc_batch_wizard_get_acc_inv_name_update"].Rows)
                        {
                            DataGridViewRow row = datagridviewContainer.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["inventory_id"].Value.ToString().Equals(dr["inventory_id"].ToString())).First();
                            if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() != "Success")
                            {
                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                MessageBox.Show(string.Format("The {0} item could not be successfully added to your list.\n\nError message:\n\n{1}"
                                    , "", dr["ExceptionMessage"].ToString()));
                            }
                            else if (dr["SavedAction"].ToString() == "Insert" && dr["SavedStatus"].ToString() == "Success")
                            {
                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                row.Cells[columnName].ErrorText = string.Empty;
                            }
                            if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() != "Success")
                            {
                                row.Cells[columnName].ErrorText = dr["ExceptionMessage"].ToString();
                                MessageBox.Show(string.Format("The {0} item could not be successfully updated for your list.\n\nError message:\n\n{1}"
                                    , "", dr["ExceptionMessage"].ToString()));
                            }
                            else if (dr["SavedAction"].ToString() == "Update" && dr["SavedStatus"].ToString() == "Success")
                            {
                                row.Cells[columnName].Style.BackColor = Color.LightBlue;
                                row.Cells[columnName].ErrorText = string.Empty;
                            }
                        }
                    }
                }
            }
        }

        private void ux_datagridviewMain2_KeyDown(object sender, KeyEventArgs e)
        {
            ProcessDGVEditShortcutKeys(ux_datagridviewSheet, e, "", null);
        }

        private void ux_buttonTemplate_Click(object sender, EventArgs e)
        {
            TemplateWizard tw = new TemplateWizard("", _sharedUtils);
            tw.Show();
        }

        private void ux_buttonClear_Click(object sender, EventArgs e)
        {
            _main2.Rows.Clear();
        }

        private void ux_checkBoxShowOnlyRowsWithError_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_checkBoxShowOnlyRowsWithError.Checked)
            {
                CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[ux_datagridviewSheet.DataSource];
                currencyManager1.SuspendBinding();
                foreach (DataGridViewRow gvrow in ux_datagridviewSheet.Rows)
                {   
                    if (gvrow.ErrorText.Equals(""))
                    {
                        bool hasError = false;
                        foreach (DataGridViewCell cell in gvrow.Cells)
                        {
                            if (!cell.ErrorText.Equals(""))
                            {
                                hasError = true;
                                break;
                            }
                        }
                        if (!hasError)
                            gvrow.Visible = false;
                    }
                }
                currencyManager1.ResumeBinding();
            }
            else {
                foreach (DataGridViewRow gvrow in ux_datagridviewSheet.Rows)
                {
                    gvrow.Visible = true;
                }
            }
        }

        private void ux_comboboxMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ux_comboboxMethod.DataSource == null) return;
            _method_id = ux_comboboxMethod.SelectedValue.ToString();
        }

        private void ux_datagridviewSheet_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStripMenu.Show(ux_datagridviewSheet, new Point(e.X, e.Y));   
            }
        }

        private void copyHeadersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < ux_datagridviewSheet.Columns.Count; i++)
            {
                if (i != 1 && i != 2)
                    builder.Append(ux_datagridviewSheet.Columns[i].HeaderText).Append("\t");
            }
            Clipboard.SetText(builder.ToString().TrimEnd(new char[] { '\t' }));
        }

        private void updateLookupTablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _sharedUtils.LookupTablesLoadTableFromDatabase("accession_lookup");
            _sharedUtils.LookupTablesLoadTableFromDatabase("inventory_lookup");
        }
    }

    class ColumnTag {
        public string Category {set; get;}
        public string TableName { set; get; }
        public ColumnTag(string category){ Category = category;}
    }
}
